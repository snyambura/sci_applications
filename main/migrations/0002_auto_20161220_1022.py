# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='adminuploadforms',
            name='form',
        ),
        migrations.RemoveField(
            model_name='formgenanswers',
            name='answer',
        ),
        migrations.RemoveField(
            model_name='formgenanswers',
            name='form',
        ),
        migrations.RemoveField(
            model_name='formgenanswers',
            name='question',
        ),
        migrations.RemoveField(
            model_name='formgendates',
            name='form',
        ),
        migrations.RemoveField(
            model_name='formgendates',
            name='question',
        ),
        migrations.RemoveField(
            model_name='formgennumeric',
            name='form',
        ),
        migrations.RemoveField(
            model_name='formgennumeric',
            name='question',
        ),
        migrations.RemoveField(
            model_name='formgentext',
            name='form',
        ),
        migrations.RemoveField(
            model_name='formgentext',
            name='question',
        ),
        migrations.DeleteModel(
            name='AdminUploadForms',
        ),
        migrations.DeleteModel(
            name='FormGenAnswers',
        ),
        migrations.DeleteModel(
            name='FormGenDates',
        ),
        migrations.DeleteModel(
            name='FormGenNumeric',
        ),
        migrations.DeleteModel(
            name='FormGenText',
        ),
        migrations.DeleteModel(
            name='Forms',
        ),
        migrations.DeleteModel(
            name='ListAnswers',
        ),
        migrations.DeleteModel(
            name='ListQuestions',
        ),
    ]
