# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20161220_1022'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AdminCaptureSites',
        ),
        migrations.DeleteModel(
            name='AdminDownload',
        ),
        migrations.RemoveField(
            model_name='adminpreferences',
            name='person',
        ),
        migrations.DeleteModel(
            name='CaptureTaskTracker',
        ),
        migrations.RemoveField(
            model_name='listreportsparameters',
            name='report',
        ),
        migrations.DeleteModel(
            name='ReportsSets',
        ),
        migrations.DeleteModel(
            name='SetupGeography',
        ),
        migrations.DeleteModel(
            name='AdminPreferences',
        ),
        migrations.DeleteModel(
            name='ListReports',
        ),
        migrations.DeleteModel(
            name='ListReportsParameters',
        ),
    ]
