# -*- coding: utf-8 -*-
"""Common method for getting related list for dropdowns... e.t.c."""
import uuid
import datetime
import collections
import itertools
import jellyfish
import traceback
import operator
from .models import SetupList
# , SetupGeography
from django.core.cache import cache
from django.core.exceptions import FieldError
from django.db.models import Q
from biodata.models import (RegisterPerson)
from main.models import SetupList
from application.models import Applications

organisation_id_prefix = 'U'
benficiary_id_prefix = 'B'
workforce_id_prefix = 'W'
form_id_prefix = 'F'
case_event_id_prefix = 'CE'


class Persons:
	id_int = None
	user_id = None
	workforce_id = None
	national_id = None
	first_name = ''
	other_names = ''
	surname = ''
	name = None
	sex_id = None
	sex = None
	gender = None
	date_of_birth = None
	date_of_death = None
	steps_ovc_number = None
	man_number = None
	ts_number = None
	sign_number = None
	roles = None
	org_units = None
	org_unit_name = None
	person_type = None
	person_type_id = None
	geo_location = None
	gdclsu_details = None
	contact = None
	registered_by_person_id = None
	direct_services = None
	

	def __init__(self, workforce_id, national_id, first_name,surname, other_names, sex_id,applicant, date_of_birth, steps_ovc_number,man_number,
				ts_number,sign_number,roles, org_units,primary_org_unit_name, person_type, gdclsu_details, contact,person_type_id, districts=None, 
				wards=None, communities=None,direct_services=None,edit_mode_hidden=None,workforce_type_change_date=None,parent_org_change_date=None,
				work_locations_change_date=None,date_of_death=None,org_data_hidden = None, primary_org_id=None, wards_string = None,
				org_units_string = None,communities_string = None):
		
		if workforce_id == fielddictionary.empty_workforce_id:
			self.user_id = 'N/A'
		else:
			self.user_id = workforce_id
		self.workforce_id = workforce_id
		self.national_id = national_id
		self.first_name = first_name
		self.surname = surname
		self.applicant = applicant
		self.other_names = other_names
		if first_name and surname:
			self.name = first_name + ' ' + surname
		self.sex_id = sex_id
		self.date_of_birth = date_of_birth
		#self.date_of_death = date_of_death
		self.steps_ovc_number = steps_ovc_number
		self.man_number = man_number
		self.ts_number = ts_number
		self.sign_number = sign_number
		#self.registered_by_person_id = registered_by_person_id
		self.roles = roles
		# self.org_units = org_units
		# self.primary_org_id = primary_org_id
		# self.primary_org_unit_name = primary_org_unit_name
		self.person_type = person_type
		#self.geo_location = geo_location
		self.gdclsu_details = gdclsu_details       
		self.contact = contact
		self.person_type_id = person_type_id
		self.wards_string = wards_string
		self.org_units_string = org_units_string
		self.communities_string = communities_string
		self.geo_location = {}
		self.geo_location = {'districts':districts,
							 'wards': wards,
							 'communities':communities}
		
		self.direct_services = direct_services
		self.edit_mode_hidden = edit_mode_hidden
		self.workforce_type_change_date=workforce_type_change_date
		self.parent_org_change_date=parent_org_change_date
		self.work_locations_change_date=work_locations_change_date
		self.date_of_death=date_of_death
		self.org_data_hidden = org_data_hidden
		_distrcits_wards = []
		_communities = None
		if wards:
			_distrcits_wards += wards
		if districts:
			_distrcits_wards += districts
			
		if _distrcits_wards:
			if self.geo_location['communities']:
				_communities = self.geo_location['communities']
			else:
				_communities = []
			self.locations_for_display = matches_for_display(_distrcits_wards, _communities)
		else:
			self.locations_for_display = []
		
		self.locations_unique_readable = []
		
		for loc in _distrcits_wards:
			self.locations_unique_readable.append(GeoLocation(loc).geo_name)
		
		# if _communities:
		# 	for comm in communities:
		# 		self.locations_unique_readable.append(RegOrgUnit.objects.get(pk=comm).org_unit_name)
		
	def __unicode__(self):
		return '%s %s'% (self.first_name, self.surname)
	
	def sex(self):
		self.sex = list_provider.get_description_for_item_id(self.sex_id)
		#self.sex = list_provider.get_item_desc_for_order_and_category(self.sex_id, fielddictionary.sex)
		if not self.sex:
			return ''
		return self.sex[0]
	
	def get_locations_for_display(self):
		return self.locations_for_display

def translate(value):
	if value:
		item_value = SetupList.objects.filter(item_id=value, is_void=False)
		item_value = item_value[0]
		return item_value.item_description
	else:
		return value

def translate_reverse(value):
	if value:
		item_value = SetupList.objects.filter(item_description=value, is_void=False)
		item_value = item_value[0]
		return item_value.item_id
	else:
		return value

def get_description_for_item_id(item_id):
	return tuple([(l.item_description) for l in SetupList.objects.filter(item_id = item_id)])

# def get_geo_list(default_txt=False):
# 	'''
# 	 Get all area_id & area_name
# 	'''
# 	initial_list = {'': default_txt} if default_txt else {}
# 	all_list = collections.OrderedDict(initial_list)
# 	try:
# 		my_list = SetupGeography.objects.filter(
# 			is_void=False).order_by('area_name')
# 		for a_list in my_list:
# 			all_list[a_list.area_id] = a_list.area_name
# 	except Exception, e:
# 		error = 'Error getting list - %s' % (str(e))
# 		print error
# 		return ()
# 	else:
# 		return all_list.items

# def get_vgeo_dict(area_id, default_txt=False):
# 	initial_list = {'': default_txt} if default_txt else {}
# 	all_list = collections.OrderedDict(initial_list)
# 	try:
# 		my_list = SetupGeography.objects.filter(area_id=area_id,
# 			is_void=False).order_by('area_name')
# 		for a_list in my_list:
# 			all_list[a_list.area_id] = a_list.area_name
# 	except Exception, e:
# 		error = 'Error getting list - %s' % (str(e))
# 		print error
# 		return ()
# 	else:
# 		return all_list.items

# def get_vgeo_list(area_id):
# 	'''
# 	Get list general filtered by field_name
# 	'''
# 	try:
# 		queryset = SetupGeography.objects.filter(area_id=area_id, is_void=False).order_by('area_id')
		
# 	except Exception, e:
# 		error = 'Error getting whole list - %s' % (str(e))
# 		print error
# 		return None
# 	else:
# 		return queryset

def get_general_list(field_names=[], item_category=False):
	'''
	Get list general filtered by field_name
	'''
	try:
		queryset = SetupList.objects.filter(is_void=False).order_by(
			'id', 'the_order')
		if len(field_names) > 1:
			q_filter = Q()
			for field_name in field_names:
				q_filter |= Q(**{"field_name": field_name})
			queryset = queryset.filter(q_filter)
		else:
			queryset = queryset.filter(field_name=field_names[0])
		if item_category:
			queryset = queryset.filter(item_category=item_category)
	except Exception, e:
		error = 'Error getting whole list - %s' % (str(e))
		print error
		return None
	else:
		return queryset


def get_list(field_name=[], default_txt=False, category=False):
	my_list = ()
	try:
		cache_key = 'set_up_list_%s' % (field_name)
		cache_list = cache.get(cache_key)
		if cache_list:
			v_list = cache_list
			print 'FROM Cache'
		else:
			print 'FROM DB'
			v_list = get_general_list([field_name], category)
			cache.set(cache_key, v_list, 300)
		my_list = v_list.values_list('item_id', 'item_description')
		if default_txt:
			initial_list = ('', default_txt)
			final_list = [initial_list] + list(my_list)
			return final_list
	except Exception, e:
		error = 'Error getting list - %s' % (str(e))
		print error
		return my_list
	else:
		return my_list



def get_dict(field_name=[], default_txt=False):
	'''
	Push the item_id and item_description into a tuple
	Instead of sorting after, ordered dict works since query
	results are already ordered from db
	'''
	# initial_list = {'': default_txt} if default_txt else {}
	# all_list = collections.OrderedDict(initial_list)
	# [{'item_id': u'TNRS', 'item_description': u'Residentia....'}
	dict_val = {}
	try:
		my_list = get_general_list(field_names=field_name)
		all_list = my_list.values('item_id', 'item_description')
		for value in all_list:
			item_id = value['item_id']
			item_details = value['item_description']
			dict_val[item_id] = item_details
	except Exception, e:
		error = 'Error getting list - %s' % (str(e))
		print error
		return {}
	else:
		return dict_val


def tokenize_search_string(search_string):
	if not search_string:
		return []
	return search_string.split()


def as_of_date_filter(queryset, as_of_date=None, include_died=True):
	"""
	as_of_date: A date or not specified. If not specified, we assume we want
	current data (date delinked is null). If specified, when looking at
	date_delinked, date_of_death e.t.c we regard them as still linked, still
	alive e.t.c if the date delinked or date_of_death occurs after this
	parameter date.
	This function takes in any queryset and tries to use the as_of_date filter
	to carry out the above rule.
	By default we need to exclude the died, but if we have include died we have
	#show all the died. If we do not have include died BUT we have
	#as of date, we get all whose date of death came after the as_of_death.
	"""
	if include_died:
		# do nothing - We have not filtered on dead or alive so everyone is
		# currently included
		pass
	else:
		# now basically DO NOT include died so we remove the died...unless the
		# as_of_date is provided then we only remove those whose date of
		# death is gt than
		if as_of_date:
			queryset = queryset.exclude(date_of_death__lt=as_of_date)
		else:
			queryset = queryset.exclude(date_of_death__isnull=False)

	if not as_of_date:
		try:
			queryset = queryset.filter(date_delinked__isnull=True)
		except FieldError:
			pass
	if as_of_date:
		try:
			queryset = queryset.exclude(date_delinked__lt=as_of_date)
		except FieldError:
			try:
				queryset = queryset.exclude(date_of_death__lt=as_of_date)
			except FieldError:
				pass

	return queryset


def order_by_relevence(wrapped_function):
	def _wrapper(*args, **kwargs):
		results = wrapped_function(*args, **kwargs)
		# we order the results by relevance
		search_string = kwargs['search_string']
		field_names = kwargs['field_names']
		diff_distances = []
		for result in results:
			# match against the concentenated fields
			field_values = [getattr(result, fname) for fname in field_names]
			field_values = itertools.ifilter(None, field_values)
			field_string = " ".join(field_values)
			# access the field names dynamically.
			diff_distance = jellyfish.jaro_distance(
				unicode(field_string.upper()),
				unicode(search_string.upper())
			)
			diff_distances.append((result, diff_distance),)
		sorted_distances = sorted(diff_distances, key=lambda x: -x[1])
		# Now return the actual sorted results not the tuples
		return [sorted_distance[0] for sorted_distance in sorted_distances]
	return _wrapper


# def get_persons_list(user, tokens, wfc_type, getJSON=False,
# 					 search_location=True,
# 					 search_wfc_by_org_unit=True):
# 	wfcs = []
# 	modelwfcs = search_wfcs(
# 		tokens=tokens, wfc_type=wfc_type,
# 		search_location=search_location,
# 		search_by_org_unit=search_wfc_by_org_unit)

# 	"""
# 	for wfc in modelwfcs:
# 		try:
# 			wfc = load_wfc_from_id(wfc.pk,user)

# 			*** To check later*** [Deals with Role Allocation]
# 			if getJSON:
# 				wfc = wfc_json(wfc,user)
# 				wfcs.append(wfc)
# 			else:
# 				wfcs.append(wfc)

# 			wfcs.append(wfc)
# 		except Exception as e:
# 			traceback.print_exc()
# 			raise Exception('Error - Retrieving person(s) failed!')
# 	"""

# 	wfcs.append(modelwfcs)

# 	return wfcs


def get_list_of_persons(search_string,
						search_string_look_in=["names", 
												# "core_ids",
											 #   "parent_orgs", 
											 #   "geo_tags"
											   ],
						# age=None,
						# has_beneficiary_id=False,
						# has_workforce_id=False,
						as_of_date=None,
						# in_person_types=[],
						number_of_results=5,
						# include_died=True, 
						sex=None,
						applicant = 0,
						include_void=False,
						search_criteria=None,
						):
	
	regpersons_queryset = Applications.objects.all()
	# if age:
	# 	regpersons_queryset = filter_age(regpersons_queryset, age, as_of_date)
	# if in_person_types:
	# 	regpersons_queryset = person_type_filter(regpersons_queryset,
	# 											 in_person_types)
	regpersons_queryset = regpersons_queryset.filter(is_approved=True)
	if applicant:
		regpersons_queryset = regpersons_queryset.filter(id=applicant)
	# if has_beneficiary_id:
	# 	regpersons_queryset = regpersons_queryset.filter(
	# 		beneficiary_id__isnull=False)
	# if has_workforce_id:
	# 	regpersons_queryset = regpersons_queryset.filter(
	# 		workforce_id__isnull=False)

	field_names = ['first_name', 'last_name']
	# Take care of criteria this - So useless
	name_results ={} 
	# core_id_results = {}
	# geo_tag_results, parent_orgs_results = {}, {}
	rank_order = ['names']
	# , 'core_ids', 'geo_tags', 'parent_orgs']
	if search_criteria == 'PSNM':
		name_results = direct_field_search(regpersons_queryset,
										   field_names=field_names,
										   search_string=search_string)
	# 	'''
	# 	core_id_results = search_external_ids(regpersons_queryset,
	# 										  search_string=search_string)
	# 	'''
	# elif search_criteria == 'PSRE':
	# 	rank_order = ['geo_tags', 'parent_orgs', 'names', 'core_ids']
	# 	geo_tag_results = search_geo_tags(regpersons_queryset, search_string)
	# elif search_criteria == 'PSOG':
	# 	rank_order = ['parent_orgs', 'names', 'core_ids', 'geo_tags']
	# 	parent_orgs_results = search_parent_orgs(regpersons_queryset,
	# 											 search_string)
	results_dict = {
		"names": name_results,
		# "core_ids": core_id_results,
		# "geo_tags": geo_tag_results,
		# "parent_orgs": parent_orgs_results,
	}
	ranked_results = rank_results(results_dict, search_string_look_in,
								  rank_order)
	return ranked_results[:number_of_results]


def new_guid_32():
	return str(uuid.uuid1()).replace('-', '')

def form_id_generator(modelid):
	uniqueid = '%05d' % modelid
	checkdigit = calculate_luhn(str(uniqueid))
	return form_id_prefix + str(uniqueid) + str(checkdigit)


def luhn_checksum(check_number):
	'''
	http://en.wikipedia.org/wiki/Luhn_algorithm
	'''
	def digits_of(n):
		return [int(d) for d in str(n)]
	digits = digits_of(check_number)
	odd_digits = digits[-1::-2]
	even_digits = digits[-2::-2]
	checksum = 0
	checksum += sum(odd_digits)
	for d in even_digits:
		checksum += sum(digits_of(d * 2))
	return checksum % 10


def is_luhn_valid(check_number):
	'''
	http://en.wikipedia.org/wiki/Luhn_algorithm
	'''
	return luhn_checksum(check_number) == 0


def calculate_luhn(partial_check_number):
	'''
	http://en.wikipedia.org/wiki/Luhn_algorithm
	'''
	check_digit = luhn_checksum(int(partial_check_number) * 10)
	return check_digit if check_digit == 0 else 10 - check_digit


def convert_date(d_string, fmt='%d-%b-%Y'):
	try:
		if isinstance(d_string, datetime.date):
			new_date = datetime.datetime.strptime(d_string, fmt)
		else:
			new_date = datetime.datetime.strptime(d_string, fmt)
	except Exception, e:
		error = 'Error converting date -%s' % (str(e))
		print error
		return d_string
	else:
		return new_date    

@order_by_relevence
def direct_field_search(queryset, field_names, search_string, as_of_date=None):
	"""Takes a queryset and a list of field names that the search string can act
	on."""
	# Split the string in case of first name, surname e.t.c
	search_strings = tokenize_search_string(search_string)
	q_filter = Q()
	for search_string in search_strings:
		for field in field_names:
			q_filter |= Q(**{"%s__icontains" % field: search_string})
	results = queryset.filter(q_filter)

	# results = as_of_date_filter(results, as_of_date=None)
	# redundant just for documentation
	# filter already applied on regpersons
	return results


# def search_geo_tags(regpersons_queryset, search_string, as_of_date=None):
# 	# geographical areas
# 	search_strings = tokenize_search_string(search_string)
# 	q_filter = Q()
# 	for search_string in search_strings:
# 		q_filter |= Q(**{"area_name__icontains": search_string})
# 	areas_matched = SetupGeography.objects.filter(q_filter)
# 	area_param = areas_matched.values_list("area_id")
# 	persons_geo = RegPersonsGeo.objects.filter(area_id__in=area_param)
# 	# persons_geo = as_of_date_filter(persons_geo, as_of_date=None)
# 	persons_param = persons_geo.values_list("person__id")
# 	matches = regpersons_queryset.filter(id__in=persons_param)

# 	return matches


def filter_age(regpersons_queryset, age=None, as_of_date=None):
	# convert the age to a timedelta
	age_datetime = datetime.timedelta(365 * int(age))
	if as_of_date:
		required_year_of_birth = as_of_date - age_datetime
	else:
		required_year_of_birth = datetime.datetime.today() - age_datetime

	one_year_time_delta = datetime.timedelta(days=365)
	results = regpersons_queryset.filter(
		date_of_birth__range=[required_year_of_birth - one_year_time_delta,
							  required_year_of_birth + one_year_time_delta])

	return results


# def person_type_filter(regpersons_queryset, passed_in_persons_types):
# 	"""in_person_types: list of person types we want to search in (tbvc, tbgr,
# 	   twvl, twne, twge), if not specified, search in all person types. if
# 	   as_of_date provided, look at records where (date_delinked is null or
# 	   date_delinked > as_of_date)
# 	"""
# 	person_types = RegPersonsTypes.objects.filter(
# 		person_type_id__in=passed_in_persons_types)
# 	regpersons_queryset = regpersons_queryset.filter(
# 		id__in=person_types.values('person'))
# 	return regpersons_queryset


def rank_results(results_dict, required_fields, rank_order):
	"""First pick out the required fields from the results dict."""
	# Choose the required items
	# Rank them and ensure no duplicates
	ranked_results = []
	for field in rank_order:
		if field in required_fields:
			try:
				field_results = results_dict[field]
				for person in field_results:
					if person not in ranked_results:
						ranked_results.append(person)
			except KeyError:
				pass
	return ranked_results

# def get_parent_area_ids(geoid, geoids=[]):
# 	geoids = [] + geoids
# 	children_ids = SetupGeography.objects.filter(parent_area_id=geoid).values_list('area_id', flat=True)
# 	if children_ids:
# 		for childid in children_ids:
# 			if childid in geoids:
# 				continue
# 			geoids.append(childid)
# 			get_parent_area_ids(childid, geoids)
# 	return geoids



def workforce_id_generator(modelid):
    uniqueid = '%05d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    return workforce_id_prefix + str(uniqueid) + str(checkdigit)


def beneficiary_id_generator(modelid):
    uniqueid = '%05d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    return benficiary_id_prefix + str(uniqueid) + str(checkdigit)



