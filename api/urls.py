"""API urls."""
from django.conf.urls import url, include
from rest_framework import routers
from .views import UserViewSet
# , OrgUnitsViewSet, CaseRecordsViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)


# Wire up our API using automatic URL routing.
urlpatterns = [
    url(r'^', include(router.urls)),
]
