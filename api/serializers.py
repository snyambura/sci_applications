"""Serializers for the test API."""
from authentication.models import AppUser
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """User serializer."""

    class Meta:
        """Overrride parameters."""

        model = AppUser
        fields = ('first_name', 'surname', 'id')


