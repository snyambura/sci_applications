
from rest_framework import viewsets
from .serializers import UserSerializer
from authentication.models import AppUser


"""Test API for Users"""
class UserViewSet(viewsets.ModelViewSet):
    """API endpoint that allows Users to be viewed or edited."""

    queryset = AppUser.objects.all().order_by('-last_login')
    serializer_class = UserSerializer


