# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CareerDetails',
            fields=[
                ('career_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('employer', models.CharField(default=None, max_length=255)),
                ('job_start_date', models.DateField(null=True)),
                ('job_end_date', models.DateField(default=None, null=True, blank=True)),
                ('job_title', models.CharField(default=None, max_length=255)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('person', models.IntegerField(default=404, null=True)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'career_details',
            },
        ),
        migrations.CreateModel(
            name='EducationDetails',
            fields=[
                ('education_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('education_level', models.CharField(max_length=255, null=True)),
                ('institution_of_study', models.CharField(max_length=255, null=True)),
                ('program', models.CharField(default=None, max_length=255)),
                ('program_start_date', models.DateField(null=True)),
                ('program_end_date', models.DateField(default=None, null=True, blank=True)),
                ('award_attained', models.CharField(default=None, max_length=255)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('person', models.IntegerField(default=404, null=True)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'education_details',
            },
        ),
        migrations.CreateModel(
            name='EmergencyContacts',
            fields=[
                ('contact_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('emergency_firstname', models.CharField(default=None, max_length=255)),
                ('emergency_surname', models.CharField(default=None, max_length=255)),
                ('emergency_relationship', models.CharField(default=None, max_length=255)),
                ('emergency_tel_no', models.IntegerField(default=None, null=True, blank=True)),
                ('emergency_email', models.EmailField(default=None, max_length=254, blank=True)),
                ('timestamp_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('person', models.IntegerField(default=404, null=True)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'emergency_contacts',
            },
        ),
        migrations.CreateModel(
            name='PersonalInformation',
            fields=[
                ('personal_info_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('person', models.IntegerField(default=404, null=True)),
                ('residence', models.CharField(max_length=255)),
                ('address', models.CharField(default=None, max_length=255)),
                ('postal_code', models.EmailField(default=None, max_length=254, blank=True)),
                ('town', models.IntegerField(default=None, null=True, blank=True)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'personal_information',
            },
        ),
        migrations.CreateModel(
            name='RegisterPerson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(default=None, max_length=255)),
                ('email', models.EmailField(default=None, max_length=254, blank=True)),
                ('phone_no', models.IntegerField(default=None, null=True, blank=True)),
                ('sex', models.CharField(default=None, max_length=4, null=True)),
                ('is_void', models.BooleanField(default=False)),
                ('created_at', models.DateField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'register_person',
                'verbose_name': 'Persons Registration',
                'verbose_name_plural': 'Persons Registrations',
            },
        ),
    ]
