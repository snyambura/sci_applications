# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('biodata', '0002_document'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registerperson',
            name='phone_no',
            field=models.CharField(default=None, max_length=13, null=True, blank=True),
        ),
    ]
