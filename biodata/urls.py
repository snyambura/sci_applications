from django.conf.urls import patterns, url


urlpatterns = patterns(

    'biodata.views',
    url(r'^education_information/$', 'education_information',
        name='education_information'),
    url(r'^manage_education/$', 'manage_education',
        name='manage_education'),
    url(r'^save_education/$', 'save_education',
        name='save_education'),
    url(r'^view_education/$', 'view_education',
        name='view_education'),
    url(r'^edit_education/$', 'edit_education',
        name='edit_education'),
    url(r'^delete_education/$', 'delete_education',
        name='delete_education'),
    url(r'^personal_information/$', 'personal_information',
        name='personal_information'),
    url(r'^career_profile/$', 'career_profile',
        name='career_profile'),
    url(r'^save_career/$', 'save_career',
        name='save_career'),
    url(r'^manage_career/$', 'manage_career',
        name='manage_career'),
    url(r'^delete_career/$', 'delete_career',
        name='delete_career'),
    url(r'^emergency_contacts/$', 'emergency_contacts',
        name='emergency_contacts'),
    url(r'^save_emergency/$', 'save_emergency',
        name='save_emergency'),
    url(r'^manage_emergency/$', 'manage_emergency',
        name='manage_emergency'),
    url(r'^save_documents/$', 'save_documents',
        name='save_documents'),
    url(r'^doc_upload/$', 'doc_upload',
        name='doc_upload'),
    url(r'^doc_delete/(?P<id>\d+)/$', 'doc_delete',
        name='doc_delete'),

    )
    