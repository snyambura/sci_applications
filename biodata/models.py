from datetime import datetime, date
from difflib import SequenceMatcher
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.dispatch import receiver
from authentication.models import AppUser
import uuid

# Create your models here.
class RegisterPerson(models.Model):
    """Model for Persons details."""
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, default=None)
    email = models.EmailField(blank=True, default=None)
    phone_no = models.CharField(null=True, blank=True, default=None, max_length=13)
    sex=models.CharField(null=True, max_length=4, default=None)

    is_void = models.BooleanField(default=False)
    # created_by = models.ForeignKey(AppUser, null=True)
    created_at = models.DateField(default=timezone.now)

    def _get_persons_data(self):
        _register_persons_data = RegisterPerson.objects.all().order_by('-id')
        return _register_persons_data

    def _get_full_name(self):
        return '%s %s %s' % (self.first_name, self.last_name)

    def make_void(self):
        """Inline call method."""
        self.is_void = True
        super(RegisterPerson, self).save()



    full_name = property(_get_full_name)


    class Meta:
        """Override table details."""

        db_table = 'register_person'
        verbose_name = 'Persons Registration'
        verbose_name_plural = 'Persons Registrations'

    def __unicode__(self):
        """To be returned by admin actions."""
        return '%s %s %s' % (self.first_name, self.last_name)


class PersonalInformation(models.Model):
    personal_info_id = models.UUIDField(
        primary_key=True, default=uuid.uuid1, editable=False)
    person=models.IntegerField(null=True, default=404)
    residence = models.CharField(max_length=255)
    address = models.CharField(max_length=255, default=None)
    postal_code = models.EmailField(blank=True, default=None)
    town = models.IntegerField(null=True, blank=True, default=None)
    is_void=models.BooleanField(default=False)

    class Meta:
        db_table = 'personal_information'
	


    
class EducationDetails(models.Model):
    # Education details
    education_id = models.UUIDField(
        primary_key=True, default=uuid.uuid1, editable=False)
    education_level = models.CharField(max_length=255, null=True)
    institution_of_study = models.CharField(max_length=255, null=True)
    program = models.CharField(max_length=255, default=None)
    program_start_date = models.DateField(null=True)
    program_end_date = models.DateField(null=True, blank=True, default=None)
    award_attained = models.CharField(max_length=255, default=None)
    timestamp_created = models.DateTimeField(default=timezone.now)
    person = models.IntegerField(null=True, default=404)
    is_void=models.BooleanField(default=False)

    class Meta:
        db_table= 'education_details'

    def _calculate_period(self):
        """Calculate period in years, then months, then days."""
        today = date.today()
        period = 0
        if self.program_start_date:
            start = self.program_start_date
            date_check = (today.month, today.day) < (start.month, start.day)
            yrs = today.year - start.year - (date_check)
            period = '%d years' % (yrs)
            if yrs == 0:
                days = (today - start).days
                mon = days / 30
                period = '%d days' % days if mon < 1 else '%d months' % mon
        return period

    period = property(_calculate_period)

class Document(models.Model):
    docfile = models.FileField(upload_to='documents/%Y/%m/%d')
    person = models.IntegerField(null=True, default=404)
    doc_type = models.CharField(max_length=4, default=None)
    is_void=models.BooleanField(default=False)
    timestamp_created = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table= 'uploaded_documents'


class CareerDetails(models.Model):
    career_id = models.UUIDField(
        primary_key=True, default=uuid.uuid1, editable=False)
    employer = models.CharField(max_length=255, default=None)
    job_start_date = models.DateField(null=True)
    job_end_date = models.DateField(null=True, blank=True, default=None)
    job_title = models.CharField(max_length=255, default=None)
    timestamp_created = models.DateTimeField(default=timezone.now)
    person = models.IntegerField(null=True, default=404)
    is_void=models.BooleanField(default=False)

    class Meta:
        db_table= 'career_details'



class EmergencyContacts(models.Model):
    contact_id = models.UUIDField(
        primary_key=True, default=uuid.uuid1, editable=False)
    emergency_firstname = models.CharField(max_length=255, default=None)
    emergency_surname = models.CharField(max_length=255, default=None)
    emergency_relationship = models.CharField(max_length=255, default=None)
    emergency_tel_no = models.IntegerField(null=True, blank=True, default=None)
    emergency_email = models.EmailField(blank=True, default=None)
    timestamp_created = models.DateTimeField(default=timezone.now)
    person = models.IntegerField(null=True, default=404)
    is_void=models.BooleanField(default=False)

    class Meta:
        db_table= 'emergency_contacts'


