from django.shortcuts import render
from datetime import datetime
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.contrib import messages
from sci_applications.views import home 
from main.functions import ( new_guid_32, get_dict, convert_date, get_list_of_persons, translate)
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from biodata.forms import (PersonalInformationForm, EducationForm, CareerForm, EmergencyContactsForm, DocumentForm)
from biodata.models import(RegisterPerson, EducationDetails, CareerDetails, EmergencyContacts, Document)
from authentication.models import AppUser


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def education_information(request):


    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    person = app_user.id
    check_fields = ['university_id, award_id', 'education_level_id']
    vals = get_dict(field_name=check_fields)
    form = EducationForm(request=request)
    forms = DocumentForm(request.POST, request.FILES)
    return render(request, 'biodata/education_information.html',
                      {'form': form, 'forms':forms, 'person': person, 'vals': vals})


def save_education(request):
    now = timezone.now()
    education_id=new_guid_32(),     

    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id

            education_level=request.POST.get('education_level')
            institution_of_study=request.POST.get('institution_of_study')
            program=request.POST.get('program')
            program_start_date=request.POST.get('program_start_date')
            program_end_date=request.POST.get('program_end_date')
            award_attained=request.POST.get('award_attained')

            if program_start_date:
                program_start_date = convert_date(program_start_date)

            if program_end_date:
                program_end_date = convert_date(program_end_date)

            if education_level:
                education_level = translate(education_level)

            if institution_of_study:
                institution_of_study = translate(institution_of_study)

            if award_attained:
                award_attained = translate(award_attained)

            if program:
                program = translate(program)

            

            EducationDetails(
                education_id=education_id,
                education_level=education_level,
                institution_of_study=institution_of_study,
                program=program,
                program_start_date=program_start_date,
                program_end_date=program_end_date,
                award_attained=award_attained,
                person=person,
                timestamp_created=now).save()

    except Exception, e:
        return HttpResponse('(%s)' % str(e))
        print 'Error saving details - %s' % str(e)
    return HttpResponse(personal_information)



def manage_education(request):
    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id
            jsonPlacementEventsData = []
            check_fields = ['university_id, award_id', 'education_level_id']
            vals = get_dict(field_name=check_fields)



            education_data = EducationDetails.objects.filter(
                person=person, is_void=False).order_by('-timestamp_created')
            if education_data:
                for edudata in education_data:

                    jsonPlacementEventsData.append({
                        'pk': edudata.education_id,
                        'person': edudata.person,
                        'education_level': edudata.education_level,
                        'institution_of_study': edudata.institution_of_study,
                        'program': edudata.program,
                        'program_start_date': (edudata.program_start_date).strftime('%d-%b-%Y'),
                        'program_end_date': (edudata.program_end_date).strftime('%d-%b-%Y'),
                        'award_attained': edudata.award_attained

                    })


    except Exception, e:
        print 'Load Education Background Error -  %s' % str(e)
    return JsonResponse(jsonPlacementEventsData,
                        content_type='application/json',
                        safe=False)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_education(request):
    jsonPlacementEventsData = []
    try:
        if request.method == 'POST':
            person = request.POST.get('person')
            education_id = request.POST.get('education_id')


            education_data = EducationDetails.objects.get(
                pk=education_id)

            # Convert Dates
            program_start_date = edudata_data.program_start_date
            if program_start_date:
                program_start_date = program_start_date.strftime('%d-%b-%Y')

            program_end_date = edudata_data.program_end_date
            if program_start_date:
                program_end_date = program_end_date.strftime('%d-%b-%Y')

                jsonPlacementEventsData.append({
                    'pk': edudata.education_id,
                    'person_id': edudata.person_id,
                    'education_level': edudata.education_level,
                    'institution_of_study': edudata.institution_of_study,
                    'program': edudata.program,
                    'program_start_date': (edudata.program_start_date).strftime('%d-%b-%Y'),
                    'program_end_date': (edudata.program_end_date).strftime('%d-%b-%Y'),
                    'award_attained': edudata.award_attained

                })

    except Exception, e:
        print 'Education Background View Error: %s' % str(e)
    return JsonResponse(jsonPlacementEventsData,
                        content_type='application/json',
                        safe=False)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def edit_education(request):
    now = timezone.now()
    try:
        if request.method == 'POST':
            action = request.POST.get('action')
            person = request.POST.get('person')
            education_id = request.POST.get('education_id')

            education_level=request.POST.get('education_level')
            institution_of_study=request.POST.get('institution_of_study')
            program=request.POST.get('program')
            program_start_date=request.POST.get('program_start_date')
            program_end_date=request.POST.get('program_end_date')
            award_attained=request.POST.get('award_attained')

            if program_start_date:
                program_start_date = convert_date(program_start_date)

            if program_end_date:
                program_end_date = convert_date(program_end_date)

                educations = EducationDetails.objects.get(
                    education_id=education_id)
                educations.education_level= education_level
                educations.institution_of_study=institution_of_study
                educations.program=program
                educations.program_start_date=program_start_date
                educations.program_end_date=program_end_date
                educations.award_attained=award_attained
               
                educations.save(update_fields=['education_level',
                                              'institution_of_study',
                                              'program',
                                              'program_start_date',
                                              'program_end_date',
                                              'award_attained'])

    except Exception, e:
        return HttpResponse('(%s)' % str(e))
        print e
    return HttpResponse(personal_information)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_education(request):
    now = timezone.now()
    try:
        if request.method == 'POST':
            pk = request.POST.get('pk')

            education = EducationDetails.objects.get(pk=pk)
            education.is_void = True
            education.save(
                update_fields=['is_void'])


    except Exception, e:
        return HttpResponse('Error - %s ' % str(e))
    return HttpResponse(personal_information)

@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def delete_career(request):
    now = timezone.now()
    try:
        if request.method == 'POST':
            pk = request.POST.get('pk')

            career = CareerDetails.objects.get(pk=pk)
            career.is_void = True
            career.save(
                update_fields=['is_void'])


    except Exception, e:
        return HttpResponse('Error - %s ' % str(e))
    return HttpResponse(personal_information)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def personal_information(request):
    try:
        if request.method == 'POST':
            # Get app_user
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            user_id = app_user.id

            residence = request.POST.get('residence')
            address = request.POST.get('address')
            postal_code = request.POST.get('postal_code')
            town = request.POST.get('town')

            PersonalInformation(
                personal_info_id=new_guid_32(),
                residence =residence,
                address=address,
                postal_code=postal_code,
                town=town,
                person=user_id,
            ).save()

            msg = 'Information successfully.'
            messages.add_message(request, messages.INFO, msg)
            redirect_url = reverse(personal_information)
            return HttpResponseRedirect(redirect_url)
    except Exception, e:
        print 'An error occured while saving - %s' % str(e)
        msg = 'Error saving Details.'
        messages.add_message(request, messages.ERROR, msg)
        redirect_url = reverse(personal_information)
        return HttpResponseRedirect(redirect_url)
    form = PersonalInformationForm()
    return render(request, 'biodata/personal_information.html', {'form': form})



@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def career_profile(request):

    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    person = app_user.id
    form = CareerForm(request=request)
    return render(request, 'biodata/career_profile.html',
                      {'form': form, 'person': person})

def save_career(request):
    now = timezone.now()
    career_id=new_guid_32(),     

    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id


            employer=request.POST.get('employer')
            job_start_date=request.POST.get('job_start_date')
            job_end_date=request.POST.get('job_end_date')
            job_title=request.POST.get('job_title')

            if job_start_date:
                job_start_date = convert_date(job_start_date)

            if job_end_date:
                job_end_date = convert_date(job_end_date)


            CareerDetails(
                career_id=career_id,
                employer=employer,
                job_start_date=job_start_date,
                job_end_date=job_end_date,
                job_title=job_title,
                person=person,
                timestamp_created=now).save()

    except Exception, e:
        print 'An error occured while saving - %s' % str(e)
        msg = 'Error saving Details.'
        messages.add_message(request, messages.ERROR, msg)
        # redirect_url = reverse(career_profile)
        # return HttpResponseRedirect(redirect_url)
    return HttpResponse(career_profile)

def manage_career(request):
    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id
            jsonCareerDetailsData = []


            career_data = CareerDetails.objects.filter(
                person=person, is_void=False).order_by('-timestamp_created')
            if career_data:
                for career in career_data:

                    jsonCareerDetailsData.append({
                        'pk': career.career_id,
                        'person': career.person,
                        'employer': career.employer,
                        'job_start_date': (career.job_start_date).strftime('%d-%b-%Y'),
                        'job_end_date': (career.job_end_date).strftime('%d-%b-%Y'),
                        'job_title': career.job_title

                    })


    except Exception, e:
        print 'Load Career Profile Error -  %s' % str(e)
    return JsonResponse(jsonCareerDetailsData,
                        content_type='application/json',
                        safe=False)


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def emergency_contacts(request):

    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    person = app_user.id

    form = EmergencyContactsForm(request=request)
    return render(request, 'biodata/emergency_contacts.html',
                      {'form': form, 'person': person})


def save_emergency(request):
    now = timezone.now()
    contact_id=new_guid_32(),     

    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id


            emergency_firstname=request.POST.get('emergency_firstname')
            emergency_surname=request.POST.get('emergency_surname')
            emergency_relationship=request.POST.get('emergency_relationship')
            emergency_tel_no=request.POST.get('emergency_tel_no')
            emergency_email=request.POST.get('emergency_email')


            EmergencyContacts(
                contact_id=contact_id,
                emergency_firstname=emergency_firstname,
                emergency_surname=emergency_surname,
                emergency_relationship=emergency_relationship,
                emergency_tel_no=emergency_tel_no,
                emergency_email=emergency_email,
                person=person,
                timestamp_created=now).save()

    except Exception, e:
        print 'An error occured while saving - %s' % str(e)
        msg = 'Error saving Details.'
        messages.add_message(request, messages.ERROR, msg)
        # redirect_url = reverse(career_profile)
        # return HttpResponseRedirect(redirect_url)
    return HttpResponse(emergency_contacts)

def manage_emergency(request):
    try:
        if request.method == 'POST':
            username = request.user.get_username()
            app_user = AppUser.objects.get(username=username)
            person = app_user.id
            jsonEmergencyContactsData = []


            emergency_contact = EmergencyContacts.objects.filter(
                person=person, is_void=False).order_by('-timestamp_created')
            if emergency_contact:
                for contacts in emergency_contact:

                    jsonEmergencyContactsData.append({
                        'pk': contacts.contact_id,
                        'person': contacts.person,
                        'emergency_firstname': contacts.emergency_firstname,
                        'emergency_surname': contacts.emergency_surname,
                        'emergency_relationship':contacts.emergency_relationship,
                        'emergency_tel_no':contacts.emergency_tel_no,
                        'emergency_email': contacts.emergency_email
                    })
                    print 'yugkhu'


    except Exception, e:
        print 'Load Career Profile Error -  %s' % str(e)
    return JsonResponse(jsonEmergencyContactsData,
                        content_type='application/json',
                        safe=False)

def save_documents(request):

    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    person = app_user.id
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            docfile=request.FILES['docfile']
            Document(
                docfile=docfile,
                person=person).save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse(education_information))
   
    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response( 'education_information.html',
        {'documents': documents, 'form': form},
        context_instance=RequestContext(request)
    )

    
def doc_upload(request):

    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    person = app_user.id
    check_fields = ['doctype_id']
    vals = get_dict(field_name=check_fields)
    
    document_type = EducationDetails.objects.filter(person=person)
    career = CareerDetails.objects.filter(person=person)

    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        doc_type = request.POST.get('doc_type')
        
        if form.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'], person=person, doc_type=doc_type)
            resume = Document.objects.filter(doc_type='DTRS', person=person)
            if resume:
                msg = 'You have submitted a CV already .'
                messages.add_message(request, messages.ERROR, msg)
                return render(request, 'biodata/doc_upload.html',
                      {'form': form, 'person': person})
            else:
                newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse(doc_upload))
    else:
        form = DocumentForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.filter(is_void=False, person=person)

    return render_to_response(
        'biodata/doc_upload.html',
        {'documents': documents, 'form': form, 'vals': vals},
        context_instance=RequestContext(request)
    )

def doc_delete(request):
    username = request.user.get_username()
    app_user = AppUser.objects.get(username=username)
    user_id = app_user.id

    documents = Document.objects.get(applicant=user_id)
    documents.is_void = True
    documents.save(update_fields=['is_void'])

    return HttpResponseRedirect(reverse(doc_upload))
