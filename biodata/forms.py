from django import forms
from django.utils.translation import ugettext_lazy as _
from main.functions import get_list
# from registry.functions import get_geo_list, get_all_geo_list

sex_id_list = get_list('sex_id', 'Please Select')
education_level_list = get_list('education_level_id', 'Please Select')
university_list=get_list('university_id', 'Please Select')
award_list = get_list('award_id', 'Please Select')
doctype_list = get_list('doctype_id', 'Please Select')
discipline_list = get_list('discipline_id', 'Please Select')

class PersonalInformationForm(forms.Form):
		residence = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Place of Residence'),
							 'class': 'form-control',
							 'id': 'residence',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group0'
							 }))
		address = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Postal Address'),
							 'class': 'form-control',
							 'id': 'address',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group0'
							 }))
		postal_code = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Postal Code'),
							 'class': 'form-control',
							 'id': 'postal_code',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group0'
							 }))
		town = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Town'),
							 'class': 'form-control',
							 'id': 'town',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group0'
							 }))


class EducationForm(forms.Form):
		def __init__(self, *args, **kwargs):
				self.request = kwargs.pop("request")
				super(EducationForm, self).__init__(*args, **kwargs)

		def clean(self):
				print self.request.user

		person = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control',
					 'id': 'person',
					 'type': 'hidden'
					 }))


		education_level = forms.ChoiceField(choices=education_level_list,
											 initial='0',
											 widget=forms.Select(
											 attrs={'placeholder': _('Appropriate Course'),
													'class': 'form-control',
													'id': 'education_level',
													# 'data-parsley-required': "true"
													}))
		institution_of_study = forms.ChoiceField(choices=university_list,
												 initial='0',
												 widget=forms.Select(
												 attrs={'placeholder': _('Appropriate Course'),
														'class': 'form-control',
														'id': 'institution_of_study',
														# 'data-parsley-required': "true"
														}))
		program = forms.ChoiceField(choices=discipline_list,
												 initial='0',
												 widget=forms.Select(
												 attrs={'placeholder': _('Appropriate Course'),
														'class': 'form-control',
														'id': 'program',
														# 'data-parsley-required': "true"
														}))
		program_start_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('Start Date'),
							 'class': 'form-control',
							 'id': 'program_start_date',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group1'
							 }))
		program_end_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('End Date'),
							 'class': 'form-control',
							 'id': 'program_end_date',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group1'
							 }))
		award_attained = forms.ChoiceField(choices=award_list,
										 initial='0',
										 widget=forms.Select(
										 attrs={'placeholder': _('Appropriate Course'),
										'class': 'form-control',
										'id': 'award_attained',
										# 'data-parsley-required': "true"
										}))
		employer = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Employer'),
							 'class': 'form-control',
							 'id': 'employer',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group2'
							 }))
		job_title = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Job Title'),
							 'class': 'form-control',
							 'id': 'job_title',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group2'
							 }))
		job_start_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('Start Date'),
							 'class': 'form-control',
							 'id': 'job_start_date',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group2'
							 }))
		job_end_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('End Date'),
							 'class': 'form-control',
							 'id': 'job_end_date',
							 # 'data-parsley-required': "true",
							 'data-parsley-group': 'group2'
							 }))

class DocumentForm(forms.Form):
	    docfile = forms.FileField(
	        label='Select a file'
	    )

	    doc_type = forms.ChoiceField(choices=doctype_list,
												 initial='0',
												 widget=forms.Select(
												 attrs={'placeholder': _(''),
														'class': 'form-control',
														'id': 'doc_type',
														'data-parsley-required': "true",
														}))
    
class CareerForm(forms.Form):
		def __init__(self, *args, **kwargs):
				self.request = kwargs.pop("request")
				super(CareerForm, self).__init__(*args, **kwargs)

		def clean(self):
				print self.request.user
		
		person = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control',
				 'id': 'person',
				 'type': 'hidden'
					 }))

		# current_job = forms.CharField(widget=forms.TextInput(
		#     attrs={'placeholder': _('Current Job'),
		#            'class': 'form-control',
		#            'id': 'current_job',
		#            'data-parsley-required': "false",
		#            'data-parsley-group': 'group2'
		#            }))
		employer = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Employer'),
							 'class': 'form-control',
							 'id': 'employer',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))
		job_title = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Job Title'),
							 'class': 'form-control',
							 'id': 'job_title',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))
		job_start_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('Start Date'),
							 'class': 'form-control',
							 'id': 'job_start_date',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))
		job_end_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('End Date'),
							 'class': 'form-control',
							 'id': 'job_end_date',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))

class EmergencyContactsForm(forms.Form):
		def __init__(self, *args, **kwargs):
				self.request = kwargs.pop("request")
				super(EmergencyContactsForm, self).__init__(*args, **kwargs)

		def clean(self):
				print self.request.user
		
		
		person = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control',
				 'id': 'person',
				 'type': 'hidden'
					 }))

		emergency_firstname = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('First Name'),
							 'class': 'form-control',
							 'id': 'award attained',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group3'
							 }))
		emergency_surname = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Surname'),
							 'class': 'form-control',
							 'id': 'award attained',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group3'
							 }))
		emergency_relationship = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Relationship'),
							 'class': 'form-control',
							 'id': 'award attained',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group3'
							 }))
		emergency_tel_no = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Phone number'),
							 'class': 'form-control',
							 'autofocus': 'true',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group3'}))
		emergency_email = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Email Address'),
							 'class': 'form-control',
							 'id': 'emergency_email',
							 'data-parsley-type': 'email',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group3'}))

		
		

