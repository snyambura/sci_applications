from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib import messages
from django.utils import timezone
from django.core import serializers
from django.conf import settings
from django.db.models import Q
import json
import operator
import random
import uuid
from datetime import datetime
from sci_applications.views import home 
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from main.functions import ( new_guid_32, get_dict, convert_date)
from exams.forms import (AddQuestionForm, TestAnswers)
from authentication.models import AppUser
from exams.models import (QuestionBank, ExamHistory, FinalScore)
from biodata.models import (CareerDetails, EducationDetails, RegisterPerson)
from application.models import Applications
from interview.models import InterviewHistory, ConsolidatedScore
from reports.forms import DateForm

# Create your views here.
@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def applications_reports(request):
	form = DateForm()
	approved = None
	disapproved = None
	reviewed = None
	unreviewed = None 
	applicants = None
	approved_program = None
	applicant_program = None
	applicants_program = None
	applicantsa_programs = None


	resultsets = []

	if request.method == 'POST':
		from_date = request.POST.get('program_start_date')
		to_date = request.POST.get('program_end_date')
		status = request.POST.get('status')
		program = request.POST.get('program')
		intake = request.POST.get('intake')

		if status == 'ASAP' and program == program and intake == intake:
			approval = Applications.objects.filter(is_approved=True, course=program, session=intake)
			for approved in approval:
				setattr(
					approved, 'first_name', str(approved.first_name))
				setattr(
					approved, 'last_name', str(approved.last_name))
				setattr(
					approved, 'course', str(approved.course))
			resultsets.append(approval)

		
		if status == 'ASAP' and program == '' and intake == '':
			approval = Applications.objects.filter(is_approved=True)
			for approved in approval:
				setattr(
					approved, 'first_name', str(approved.first_name))
				setattr(
					approved, 'last_name', str(approved.last_name))
				setattr(
					approved, 'course', str(approved.course))
			resultsets.append(approval)

		if status == 'ASAP' and program == '' and intake == intake:
			approval = Applications.objects.filter(is_approved=True, session=intake)
			for approved in approval:
				setattr(
					approved, 'first_name', str(approved.first_name))
				setattr(
					approved, 'last_name', str(approved.last_name))
				setattr(
					approved, 'course', str(approved.course))
			resultsets.append(approval)

		if status == 'ASAP' and program == program and intake == '':
			approval = Applications.objects.filter(is_approved=True, course=program)
			for approved in approval:
				setattr(
					approved, 'first_name', str(approved.first_name))
				setattr(
					approved, 'last_name', str(approved.last_name))
				setattr(
					approved, 'course', str(approved.course))
			resultsets.append(approval)

		if status == 'ASDA' and program == program and intake == intake:
			disapproval = Applications.objects.filter(is_void=True, is_approved=False, course=program, session=intake)
			for disapproved in disapproval:
				setattr(
					disapproved, 'first_name', str(disapproved.first_name))
				setattr(
					disapproved, 'last_name', str(disapproved.last_name))
				setattr(
					disapproved, 'course', str(disapproved.course))
			resultsets.append(disapproval)

		if status == 'ASDA' and program == '' and intake == '':
			disapproval = Applications.objects.filter(is_void=True, is_approved=False)
			for disapproved in disapproval:
				setattr(
					disapproved, 'first_name', str(disapproved.first_name))
				setattr(
					disapproved, 'last_name', str(disapproved.last_name))
				setattr(
					disapproved, 'course', str(disapproved.course))
			resultsets.append(disapproval)

		if status == 'ASDA' and program == '' and intake == intake:
			disapproval = Applications.objects.filter(is_void=True, is_approved=False, session=intake)
			for disapproved in disapproval:
				setattr(
					disapproved, 'first_name', str(disapproved.first_name))
				setattr(
					disapproved, 'last_name', str(disapproved.last_name))
				setattr(
					disapproved, 'course', str(disapproved.course))
			resultsets.append(disapproval)

		if status == 'ASDA' and program == program and intake == '':
			disapproval = Applications.objects.filter(is_void=True, is_approved=False, course=program)
			for disapproved in disapproval:
				setattr(
					disapproved, 'first_name', str(disapproved.first_name))
				setattr(
					disapproved, 'last_name', str(disapproved.last_name))
				setattr(
					disapproved, 'course', str(disapproved.course))
			resultsets.append(disapproval)


		if status == 'ASAR' and program == program and intake == intake:
			review = Applications.objects.filter(is_void = True, course=program, session=intake)
			for reviewed in review:
				setattr(
					reviewed, 'first_name', str(reviewed.first_name))
				setattr(
					reviewed, 'last_name', str(reviewed.last_name))
				setattr(
					reviewed, 'course', str(reviewed.course))
			resultsets.append(review)


		if status == 'ASAR' and program == '' and intake == '':
			review = Applications.objects.filter(is_void = True)
			for reviewed in review:
				setattr(
					reviewed, 'first_name', str(reviewed.first_name))
				setattr(
					reviewed, 'last_name', str(reviewed.last_name))
				setattr(
					reviewed, 'course', str(reviewed.course))
			resultsets.append(review)

		if status == 'ASAR' and program == '' and intake == intake:
			review = Applications.objects.filter(is_void = True, session=intake)
			for reviewed in review:
				setattr(
					reviewed, 'first_name', str(reviewed.first_name))
				setattr(
					reviewed, 'last_name', str(reviewed.last_name))
				setattr(
					reviewed, 'course', str(reviewed.course))
			resultsets.append(review)

		if status == 'ASAR' and program == program and intake == '':
			review = Applications.objects.filter(is_void = True, course=program)
			for reviewed in review:
				setattr(
					reviewed, 'first_name', str(reviewed.first_name))
				setattr(
					reviewed, 'last_name', str(reviewed.last_name))
				setattr(
					reviewed, 'course', str(reviewed.course))
			resultsets.append(review)

		if status == 'ASUV' and program == program and intake == intake:
			unreview = Applications.objects.filter(is_void=False, course=program, session=intake)
			for unreviewed in unreview:
				setattr(
					unreviewed, 'first_name', str(unreviewed.first_name))
				setattr(
					unreviewed, 'last_name', str(unreviewed.last_name))
				setattr(
					unreviewed, 'course', str(unreviewed.course))
			resultsets.append(unreview)

		if status == 'ASUV' and program == '' and intake == '':
			unreview = Applications.objects.filter(is_void=False)
			for unreviewed in unreview:
				setattr(
					unreviewed, 'first_name', str(unreviewed.first_name))
				setattr(
					unreviewed, 'last_name', str(unreviewed.last_name))
				setattr(
					unreviewed, 'course', str(unreviewed.course))
			resultsets.append(unreview)


		if status == 'ASUV' and program == '' and intake == intake:
			unreview = Applications.objects.filter(is_void=False, session=intake)
			for unreviewed in unreview:
				setattr(
					unreviewed, 'first_name', str(unreviewed.first_name))
				setattr(
					unreviewed, 'last_name', str(unreviewed.last_name))
				setattr(
					unreviewed, 'course', str(unreviewed.course))
			resultsets.append(unreview)


		if status == 'ASUV' and program == program and intake == '':
			unreview = Applications.objects.filter(is_void=False, course=program)
			for unreviewed in unreview:
				setattr(
					unreviewed, 'first_name', str(unreviewed.first_name))
				setattr(
					unreviewed, 'last_name', str(unreviewed.last_name))
				setattr(
					unreviewed, 'course', str(unreviewed.course))
			resultsets.append(unreview)




		if status == '' and program == program and intake == intake:
			applicantss = Applications.objects.filter(course=program, session=intake)
			for applicants in applicantss:
				setattr(
					applicants, 'first_name', str(applicants.first_name))
				setattr(
					applicants, 'last_name', str(applicants.last_name))
				setattr(
					applicants, 'course', str(applicants.course))
			resultsets.append(applicantss)

		if status == '' and program == '' and intake == intake:
			applicantss = Applications.objects.filter(session=intake)
			for applicants in applicantss:
				setattr(
					applicants, 'first_name', str(applicants.first_name))
				setattr(
					applicants, 'last_name', str(applicants.last_name))
				setattr(
					applicants, 'course', str(applicants.course))
			resultsets.append(applicantss)

		if status == '' and program == program and intake == '':
			applicantss = Applications.objects.filter(course=program)
			for applicants in applicantss:
				setattr(
					applicants, 'first_name', str(applicants.first_name))
				setattr(
					applicants, 'last_name', str(applicants.last_name))
				setattr(
					applicants, 'course', str(applicants.course))
			resultsets.append(applicantss)

		
	else:
		applicanta = Applications.objects.all()
		for applicant in applicanta:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
		resultsets.append(applicanta)



	msg = 'Showing all applications'
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'reports/applications_reports.html',
				  {
				  'vals': vals,
				  'form' :form,
				  'resultsets': resultsets
				   })

def interviews_reports(request):
	form = DateForm()
	approved = None
	disapproved = None
	reviewed = None
	unreviewed = None 
	applicants = None
	approved_program = None
	applicant_program = None


	resultsets = []

	if request.method == 'POST':
		from_date = request.POST.get('program_start_date')
		to_date = request.POST.get('program_end_date')
		status = request.POST.get('status')
		program = request.POST.get('program')

		# if program == program:
		# 	approval = ConsolidatedScore.objects.filter(is_approved=True, program=program)
		# 	for approved in approval:
		# 		setattr(
		# 			approved, 'first_name', str(approved.first_name))
		# 		setattr(
		# 			approved, 'last_name', str(approved.last_name))
		# 		setattr(
		# 			approved, 'score', str(approved.average_score))
		# 	resultsets.append(approval)

	else:
		applicanta = ConsolidatedScore.objects.all().order_by('-average_score')
		for applicant in applicanta:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'score', str(applicant.average_score))
		resultsets.append(applicanta)



	msg = 'Showing all interviews'
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'reports/interviews_reports.html',
				  {
				  'vals': vals,
				  'form' :form,
				  'resultsets': resultsets
				   })

def test_reports(request):
	form = DateForm()
	approved = None
	disapproved = None
	reviewed = None
	unreviewed = None 
	applicants = None
	approved_program = None
	applicant_program = None


	resultsets = []

	if request.method == 'POST':
		from_date = request.POST.get('program_start_date')
		to_date = request.POST.get('program_end_date')
		status = request.POST.get('status')
		program = request.POST.get('program')
		passed = request.POST.get('passed')

		if passed == 'TSPS' and program == program:
			passing = ExamHistory.objects.filter(has_passed=True, course=program).order_by('-exam_score')
			for applicant in passing:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
			resultsets.append(passing)

		if passed == 'TSPS' and program == '':
			passing = ExamHistory.objects.filter(has_passed=True).order_by('-exam_score')
			for applicant in passing:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
			resultsets.append(passing)

		if passed == 'TSFL' and program == program:
			passing = ExamHistory.objects.filter(has_passed=False, course=program).order_by('-exam_score')
			for applicant in passing:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
			resultsets.append(passing)

		if passed == 'TSFL' and program == '':
			passing = ExamHistory.objects.filter(has_passed=False).order_by('-exam_score')
			for applicant in passing:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
			resultsets.append(passing)

		if passed == '' and program == program:
			passing = ExamHistory.objects.filter(course=program).order_by('-exam_score')
			for applicant in passing:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
			resultsets.append(passing)



	else:
		applicanta = ExamHistory.objects.all().order_by('-exam_score')
		for applicant in applicanta:
				setattr(
					applicant, 'first_name', str(applicant.first_name))
				setattr(
					applicant, 'last_name', str(applicant.last_name))
				setattr(
					applicant, 'course', str(applicant.course))
				setattr(
					applicant, 'score', str(applicant.exam_score))
		resultsets.append(applicanta)



	msg = 'Showing all test results'
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'reports/test_reports.html',
				  {
				  'vals': vals,
				  'form' :form,
				  'resultsets': resultsets
				   })

def view_consolidated_scores(request):
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	assessed = FinalScore.objects.filter(is_void=False).order_by('-total_scores')
	
	msg = 'Showing results '
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'reports/view_consolidated_scores.html',
				  {'assessed':assessed

					   })


# def convert_to_json(request):
# 	username = request.user.get_username()
# 	app_user = AppUser.objects.get(username=username)
# 	user_id = app_user.id

# 	data = select row_to_json(Applications) from Applications
# 	jsonData = json.dumps(data)
# 	print(jsonData)
	 
# 	# Writing JSON data into a file called JSONData.json
# 	#Use the method called json.dump()
# 	#It's just dump() and not dumps()
# 	#Encode JSON data
# 	with open('JSONData.json', 'w') as f:
# 	     json.dump(jsonData, f)


# def get_data(request):
# 	username = request.user.get_username()
# 	app_user = AppUser.objects.get(username=username)
# 	user_id = app_user.id

# 	string = file_get_contents("JsonData.json")
# 	print string


