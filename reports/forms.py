from django import forms
from django.utils.translation import ugettext_lazy as _
from main.functions import get_list

time_frame_list = get_list('time_frame_id', 'Select time frame')
status_list = get_list('status_id', 'Filter by application status')
course_list = get_list('masters_program_id', 'Filter by MSc Program')
intake_list = get_list('intake_id', 'Filter by Intake')
passed_list = get_list('test_id', 'Filter by Passmark')

class DateForm(forms.Form):
		program_start_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('From Date'),
							 'class': 'form-control',
							 'id': 'program_start_date',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group1'
							 }))
		program_end_date = forms.DateField(widget=forms.TextInput(
				attrs={'placeholder': _('To Date'),
							 'class': 'form-control',
							 'id': 'program_end_date',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group1'
							 }))
		time_frame = forms.ChoiceField(choices=time_frame_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'time_frame',
												}))
		status = forms.ChoiceField(choices=status_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'status',
												}))
		program = forms.ChoiceField(choices=course_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'status',
												}))
		intake = forms.ChoiceField(choices=intake_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'intake',
												}))
		passed = forms.ChoiceField(choices=passed_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'passed',
												}))