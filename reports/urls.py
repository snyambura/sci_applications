from django.conf.urls import patterns, url


urlpatterns = patterns(

    'reports.views',
    url(r'^applications_reports/$', 'applications_reports', name='applications_reports'),
    url(r'^interviews_reports/$', 'interviews_reports', name='interviews_reports'),
    url(r'^test_reports/$', 'test_reports', name='test_reports'),
    url(r'^view_consolidated_scores/$', 'view_consolidated_scores', name='view_consolidated_scores'),

    )