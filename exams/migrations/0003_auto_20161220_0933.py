# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0002_examhistory_course'),
    ]

    operations = [
        migrations.AddField(
            model_name='examhistory',
            name='answer10',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer11',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer12',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer13',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer14',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer15',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer9',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer10',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer11',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer12',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer13',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer14',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer15',
            field=models.CharField(default=None, max_length=4),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer9',
            field=models.CharField(default=None, max_length=4),
        ),
    ]
