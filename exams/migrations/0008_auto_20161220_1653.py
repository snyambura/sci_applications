# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0007_auto_20161220_1642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examhistory',
            name='applicant',
            field=models.IntegerField(default=404, null=True),
        ),
        migrations.AlterField(
            model_name='finalscore',
            name='applicant',
            field=models.IntegerField(default=404, null=True),
        ),
    ]
