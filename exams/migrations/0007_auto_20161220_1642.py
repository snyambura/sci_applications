# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0006_finalscore'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examhistory',
            name='applicant',
            field=models.ForeignKey(default=2, to='biodata.RegisterPerson'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='finalscore',
            name='applicant',
            field=models.ForeignKey(default=1, to='biodata.RegisterPerson'),
            preserve_default=False,
        ),
    ]
