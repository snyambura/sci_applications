# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0003_auto_20161220_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer10',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer11',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer12',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer13',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer14',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer15',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='examhistory',
            name='correct_answer9',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
    ]
