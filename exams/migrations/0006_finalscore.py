# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0005_delete_finalscore'),
    ]

    operations = [
        migrations.CreateModel(
            name='FinalScore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=None, max_length=50)),
                ('last_name', models.CharField(default=None, max_length=50)),
                ('applicant', models.IntegerField(default=404, null=True)),
                ('exam_score', models.IntegerField(default=0)),
                ('interview_score', models.IntegerField(default=0)),
                ('timestamp_updated', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'final_score',
            },
        ),
    ]
