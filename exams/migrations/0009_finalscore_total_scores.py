# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0008_auto_20161220_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='finalscore',
            name='total_scores',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
