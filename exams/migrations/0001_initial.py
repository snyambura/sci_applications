# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ExamHistory',
            fields=[
                ('test_id', models.UUIDField(default=uuid.uuid1, serialize=False, editable=False, primary_key=True)),
                ('applicant', models.IntegerField(default=404, null=True)),
                ('first_name', models.CharField(max_length=5000, null=True)),
                ('last_name', models.CharField(max_length=5000, null=True)),
                ('correct_answer1', models.CharField(default=None, max_length=4)),
                ('answer1', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer2', models.CharField(default=None, max_length=4)),
                ('answer2', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer3', models.CharField(default=None, max_length=4)),
                ('answer3', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer4', models.CharField(default=None, max_length=4)),
                ('answer4', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer5', models.CharField(default=None, max_length=4)),
                ('answer5', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer6', models.CharField(default=None, max_length=4)),
                ('answer6', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer7', models.CharField(default=None, max_length=4)),
                ('answer7', models.CharField(default=None, max_length=4, null=True)),
                ('correct_answer8', models.CharField(default=None, max_length=4)),
                ('answer8', models.CharField(default=None, max_length=4, null=True)),
                ('exam_score', models.IntegerField(default=0)),
                ('timestamp_submitted', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
                ('has_passed', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'exam_history',
            },
        ),
        migrations.CreateModel(
            name='FinalScore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=None, max_length=50)),
                ('last_name', models.CharField(default=None, max_length=50)),
                ('applicant', models.IntegerField(default=404, null=True)),
                ('exam_score', models.IntegerField(default=0)),
                ('interview_score', models.IntegerField(default=0)),
                ('timestamp_updated', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'final_score',
            },
        ),
        migrations.CreateModel(
            name='QuestionBank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course', models.CharField(max_length=255, null=True)),
                ('question', models.CharField(max_length=2550, null=True)),
                ('choice_a', models.CharField(default=None, max_length=2550)),
                ('choice_b', models.CharField(max_length=2550, null=True)),
                ('choice_c', models.CharField(max_length=2550, null=True)),
                ('choice_d', models.CharField(max_length=2550, null=True)),
                ('correct_answer', models.CharField(max_length=5, null=True)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'question_bank',
            },
        ),
    ]
