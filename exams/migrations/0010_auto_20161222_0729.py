# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0009_finalscore_total_scores'),
    ]

    operations = [
        migrations.AddField(
            model_name='examhistory',
            name='answer16',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer17',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer18',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer19',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer20',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer21',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer22',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer23',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer24',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='answer25',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer16',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer17',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer18',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer19',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer20',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer21',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer22',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer23',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer24',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='examhistory',
            name='correct_answer25',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
    ]
