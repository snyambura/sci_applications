from django import forms
from django.utils.translation import ugettext_lazy as _
from main.functions import get_list

course_list=get_list('masters_program_id', 'Please Select')
choices_list=get_list('choice_id', 'Please Select')


class AddQuestionForm(forms.Form):
	 

		course = forms.ChoiceField(choices=course_list,
				 initial='0',
				 widget=forms.Select(
				 attrs={'placeholder': _('Appropriate Course'),
								'class': 'form-control',
								'id': 'course',
								}))
		question = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Enter Question'),
							 'class': 'form-control',
							 'id': 'question',
							 'data-parsely-required': "true"
							 }))
		choice_a = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Choice A'),
							 'class': 'form-control',
							 'id': 'choice_a',
							 'data-parsely-required': "true"
							 }))
		choice_b = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Choice B'),
							 'class': 'form-control',
							 'id': 'choice_b',
							 'data-parsely-required': "true"
							 }))
		choice_c = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Choice C'),
							 'class': 'form-control',
							 'id': 'choice_c',
							 'data-parsely-required': "true"
							 }))
		choice_d = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Choice D'),
							 'class': 'form-control',
							 'id': 'choice_d',
							 'data-parsely-required': "true"
							 }))
		correct_answer = forms.ChoiceField(choices=choices_list,
															 initial='0',
															 widget=forms.Select(
															 attrs={
																			'class': 'form-control',
																			'id': 'correct_answer',
																			'data-parsely-required': "true"
																			}))


class TestAnswers(forms.Form):
		# answer1 = forms.CharField(
		# 		required=True,
		# 		widget=forms.RadioSelect(
		# 				attrs={'id': 'answer1'}))
		answer1 = forms.ChoiceField(choices=choices_list,
		                           initial='0',
		                           widget=forms.Select(
		                           attrs={
		                                  'class': 'form-control',
		                                  'id': 'answer1',
		                                  'data-parsely-required': "true"
		                                  }))
		answer2 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
													'class': 'form-control',
													'id': 'answer2',
													'data-parsely-required': "true"
													}))
		answer3 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
													'class': 'form-control',
													'id': 'answer3',
													'data-parsely-required': "true"
													}))
		answer4 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
													'class': 'form-control',
													'id': 'answer4',
													'data-parsely-required': "true"
													}))
		answer5 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer5',
											'data-parsely-required': "true"
											}))
		answer6 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer6',
											'data-parsely-required': "true"
											}))
		answer7 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer7',
											'data-parsely-required': "true"
											}))
		answer8 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer8',
											'data-parsely-required': "true"
											}))

		answer9 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer9',
											'data-parsely-required': "true"
											}))

		answer10 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer10',
											'data-parsely-required': "true"
											}))

		answer11 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer11',
											'data-parsely-required': "true"
											}))

		answer12 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer12',
											'data-parsely-required': "true"
											}))

		answer13 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer13',
											'data-parsely-required': "true"
											}))

		answer14 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer14',
											'data-parsely-required': "true"
											}))

		answer15 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer15',
											'data-parsely-required': "true"
											}))

		answer16 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer16',
											'data-parsely-required': "true"
											}))

		answer17 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer17',
											'data-parsely-required': "true"
											}))

		answer18 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer18',
											'data-parsely-required': "true"
											}))

		answer19 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer19',
											'data-parsely-required': "true"
											}))

		answer20 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer20',
											'data-parsely-required': "true"
											}))

		answer21 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer21',
											'data-parsely-required': "true"
											}))

		answer22 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer22',
											'data-parsely-required': "true"
											}))

		answer23 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer23',
											'data-parsely-required': "true"
											}))

		answer24 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer24',
											'data-parsely-required': "true"
											}))

		answer25 = forms.ChoiceField(choices=choices_list,
									 initial='0',
									 widget=forms.Select(
									 attrs={
											'class': 'form-control',
											'id': 'answer25',
											'data-parsely-required': "true"
											}))
		

