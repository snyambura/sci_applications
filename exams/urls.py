from django.conf.urls import patterns, url


urlpatterns = patterns(

    'exams.views',
    url(r'^add_question/$', 'add_question', name='add_question'),
    url(r'^sit_test/$', 'sit_test', name='sit_test'),
    url(r'^calculate_score/$', 'calculate_score', name='calculate_score'),
    url(r'^display_score/$', 'display_score', name='display_score'),
    url(r'^view_exams/$', 'view_exams', name='view_exams'),
    url(r'^entry_test/$', 'entry_test', name='entry_test'),
    )
    