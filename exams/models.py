from datetime import datetime, date
from difflib import SequenceMatcher
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.conf import settings
from django.dispatch import receiver
import uuid
from authentication.models import AppUser

# Create your models here.

class QuestionBank(models.Model):
	# Education details
	course = models.CharField(max_length=255, null=True)
	question = models.CharField(max_length=2550, null=True)
	choice_a = models.CharField(max_length=2550, default=None)
	choice_b = models.CharField(max_length=2550, null=True)
	choice_c = models.CharField(max_length=2550, null=True)
	choice_d = models.CharField(max_length=2550, null=True)
	correct_answer = models.CharField(max_length=5, null=True)
	is_void=models.BooleanField(default=False)

	class Meta:
		db_table= 'question_bank'

class ExamHistory(models.Model):
	test_id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	applicant = models.IntegerField(null=True, default=404)
	first_name = models.CharField(max_length=5000, null=True)
	last_name = models.CharField(max_length=5000, null=True)
	course = models.CharField(max_length=255, null=True)
	correct_answer1 = models.CharField(max_length=4, default=None)
	answer1 = models.CharField(max_length=4, null=True, default=None)
	correct_answer2= models.CharField(max_length=4, default=None)
	answer2 = models.CharField(max_length=4,null=True, default=None)
	correct_answer3= models.CharField(max_length=4, default=None)
	answer3 = models.CharField(max_length=4, null=True, default=None)
	correct_answer4 = models.CharField(max_length=4, default=None)
	answer4 = models.CharField(max_length=4, null=True, default=None)
	correct_answer5 = models.CharField(max_length=4, default=None)
	answer5 = models.CharField(max_length=4,null=True, default=None)
	correct_answer6 = models.CharField(max_length=4, default=None)
	answer6 = models.CharField(max_length=4,null=True, default=None)
	correct_answer7 = models.CharField(max_length=4, default=None)
	answer7 = models.CharField(max_length=4,null=True, default=None)
	correct_answer8 = models.CharField(max_length=4, default=None)
	answer8 = models.CharField(max_length=4,null=True, default=None)
	correct_answer9 = models.CharField(max_length=4, null=True, default=None)
	answer9 = models.CharField(max_length=4,null=True, default=None)
	correct_answer10 = models.CharField(max_length=4, null=True, default=None)
	answer10 = models.CharField(max_length=4, null=True, default=None)
	correct_answer11 = models.CharField(max_length=4, null=True, default=None)
	answer11 = models.CharField(max_length=4,null=True, default=None)
	correct_answer12 = models.CharField(max_length=4, null=True, default=None)
	answer12 = models.CharField(max_length=4,null=True, default=None)
	correct_answer13 = models.CharField(max_length=4, null=True, default=None)
	answer13 = models.CharField(max_length=4,null=True, default=None)
	correct_answer14 = models.CharField(max_length=4, null=True, default=None)
	answer14 = models.CharField(max_length=4,null=True, default=None)
	correct_answer15 = models.CharField(max_length=4, null=True, default=None)
	answer15 = models.CharField(max_length=4,null=True, default=None)
	correct_answer16 = models.CharField(max_length=4, null=True, default=None)
	answer16 = models.CharField(max_length=4,null=True, default=None)
	correct_answer17 = models.CharField(max_length=4, null=True, default=None)
	answer17 = models.CharField(max_length=4,null=True, default=None)
	correct_answer18 = models.CharField(max_length=4, null=True, default=None)
	answer18 = models.CharField(max_length=4,null=True, default=None)
	correct_answer19 = models.CharField(max_length=4, null=True, default=None)
	answer19 = models.CharField(max_length=4,null=True, default=None)
	correct_answer20 = models.CharField(max_length=4, null=True, default=None)
	answer20 = models.CharField(max_length=4,null=True, default=None)
	correct_answer21 = models.CharField(max_length=4, null=True, default=None)
	answer21 = models.CharField(max_length=4,null=True, default=None)
	correct_answer22 = models.CharField(max_length=4, null=True, default=None)
	answer22 = models.CharField(max_length=4,null=True, default=None)
	correct_answer23 = models.CharField(max_length=4, null=True, default=None)
	answer23 = models.CharField(max_length=4,null=True, default=None)
	correct_answer24 = models.CharField(max_length=4, null=True, default=None)
	answer24 = models.CharField(max_length=4,null=True, default=None)
	correct_answer25 = models.CharField(max_length=4, null=True, default=None)
	answer25 = models.CharField(max_length=4,null=True, default=None)
	exam_score = models.IntegerField( default = 0)	
	timestamp_submitted = models. DateTimeField(default=timezone.now)
	is_void = models.BooleanField(default=False)
	has_passed = models.BooleanField(default=False)


	class Meta:
		db_table = 'exam_history'


class FinalScore(models.Model):
	first_name = models.CharField(max_length=50, default=None)
	last_name = models.CharField(max_length=50, default=None)
	applicant = models.IntegerField(null=True, default=404)
	exam_score = models.IntegerField(default=0)
	interview_score = models.IntegerField(default=0)
	total_scores = models.IntegerField(default=0, null=True)
	timestamp_updated= models.DateTimeField(default=timezone.now)
	is_void = models.BooleanField(default=False)

	class Meta:
		db_table= 'final_score'

	

