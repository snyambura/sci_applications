from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib import messages
from django.utils import timezone
from django.core import serializers
from django.conf import settings
from django.db.models import Q
import json
import operator
import random
import uuid
from datetime import datetime
from sci_applications.views import home 
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from main.functions import ( new_guid_32, get_dict, convert_date)
from exams.forms import (AddQuestionForm, TestAnswers)
from authentication.models import AppUser
from exams.models import (QuestionBank, ExamHistory, FinalScore)
from biodata.models import (CareerDetails, EducationDetails, RegisterPerson)
from application.models import Applications

# Create your views here.

@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def add_question(request):
	try:
		if request.method == 'POST':
			# Get app_user
			username = request.user.get_username()
			app_user = AppUser.objects.get(username=username)
			user_id = app_user.id

			course = request.POST.get('course')
			question = request.POST.get('question')
			choice_a = request.POST.get('choice_a')
			choice_b = request.POST.get('choice_b')
			choice_c = request.POST.get('choice_c')
			choice_d = request.POST.get('choice_d')
			correct_answer = request.POST.get('correct_answer')

			QuestionBank(
				course=course,
				question=question,
				choice_a=choice_a,
				choice_b=choice_b,
				choice_c=choice_c,
				choice_d=choice_d,
				correct_answer=correct_answer,
			).save()

			msg = 'Question saved successfully.'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(add_question)
			return HttpResponseRedirect(redirect_url)
	except Exception, e:
		print 'An error occured while saving - %s' % str(e)
		msg = 'Error saving Question.'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(add_question)
		return HttpResponseRedirect(redirect_url)
	form = AddQuestionForm()
	return render(request, 'exams/add_question.html', {'form': form})



@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def sit_test(request):
	try:
		username = request.user.get_username()
		app_user = AppUser.objects.get(username=username)
		user_id = app_user.id
		test_id = new_guid_32()

		init_data = RegisterPerson.objects.get(pk=user_id)
		first_name = init_data.first_name
		last_name = init_data.last_name
		programs = Applications.objects.get(applicant=user_id)
		program = programs.course

		question_x= QuestionBank.objects.filter(course=program)
		# questions = question_x.order_by('?')[:7]
		pool= list( question_x)
		random.shuffle( pool )
		questions = pool[:25]

		test_questions = questions[0]
		test_questions2 = questions[1]
		test_questions3 = questions[2]
		test_questions4 = questions[3]
		test_questions5 = questions[4]
		test_questions6 = questions[5]
		test_questions7 = questions[6]
		test_questions8 = questions[7]
		test_questions9 = questions[8]
		test_questions10 = questions[9]
		test_questions11 = questions[10]
		test_questions12 = questions[11]
		test_questions13 = questions[12]
		test_questions14 = questions[13]
		test_questions15 = questions[14]
		test_questions16 = questions[15]
		test_questions17 = questions[16]
		test_questions18 = questions[17]
		test_questions19 = questions[18]
		test_questions20 = questions[19]
		test_questions21 = questions[20]
		test_questions22 = questions[21]
		test_questions23 = questions[22]
		test_questions24 = questions[23]
		test_questions25 = questions[24]

		examined=ExamHistory.objects.filter(applicant=user_id, is_void=True)
		if examined:
			msg = 'You have done an entry test already .'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(display_score)
			return HttpResponseRedirect(redirect_url)

		else:
			ExamHistory(
					test_id=test_id,
					correct_answer1=test_questions.correct_answer,
					correct_answer2=test_questions2.correct_answer,
					correct_answer3=test_questions3.correct_answer,
					correct_answer4=test_questions4.correct_answer,
					correct_answer5=test_questions5.correct_answer,
					correct_answer6=test_questions6.correct_answer,
					correct_answer7=test_questions7.correct_answer,
					correct_answer8=test_questions8.correct_answer,
					correct_answer9=test_questions9.correct_answer,
					correct_answer10=test_questions10.correct_answer,
					correct_answer11=test_questions11.correct_answer,
					correct_answer12=test_questions12.correct_answer,
					correct_answer13=test_questions13.correct_answer,
					correct_answer14=test_questions14.correct_answer,
					correct_answer15=test_questions15.correct_answer,
					correct_answer16=test_questions16.correct_answer,
					correct_answer17=test_questions17.correct_answer,
					correct_answer18=test_questions18.correct_answer,
					correct_answer19=test_questions19.correct_answer,
					correct_answer20=test_questions20.correct_answer,
					correct_answer21=test_questions21.correct_answer,
					correct_answer22=test_questions22.correct_answer,
					correct_answer23=test_questions23.correct_answer,
					correct_answer24=test_questions24.correct_answer,
					correct_answer25=test_questions25.correct_answer,
					course = program,
					applicant=user_id,
					first_name=first_name,
					last_name=last_name
					).save()

		
	except Exception, e:
		msg = 'Display test error - %s' % str(e)
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(entry_test)
		return HttpResponseRedirect(redirect_url)

	form = TestAnswers()
	return render(request,
					  'exams/sit_test.html',
					  {'test_questions': test_questions,
					   'test_questions2': test_questions2,
					   'test_questions3': test_questions3,
					   'test_questions4': test_questions4,
					   'test_questions5': test_questions5,
					   'test_questions6': test_questions6,
					   'test_questions7': test_questions7,
					   'test_questions8': test_questions8,
					   'test_questions9': test_questions9,
					   'test_questions10': test_questions10,
					   'test_questions11': test_questions11,
					   'test_questions12': test_questions12,
					   'test_questions13': test_questions13,
					   'test_questions14': test_questions14,
					   'test_questions15': test_questions15,
					   'test_questions16': test_questions16,
					   'test_questions17': test_questions17,
					   'test_questions18': test_questions18,
					   'test_questions19': test_questions19,
					   'test_questions20': test_questions20,
					   'test_questions21': test_questions21,
					   'test_questions22': test_questions22,
					   'test_questions23': test_questions23,
					   'test_questions24': test_questions24,
					   'test_questions25': test_questions25,
						'form':form,
						'user_id':user_id,
						'test_id': test_id
						})
		

	
@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def calculate_score(request):
	try:
		username = request.user.get_username()
		app_user = AppUser.objects.get(username=username)
		user_id = app_user.id
		qsn1 = 0
		qsn2 = 0
		qsn3 = 0
		qsn4 = 0
		qsn5 = 0
		qsn6 = 0
		qsn7 = 0
		qsn8 = 0
		qsn9 = 0
		qsn10 = 0
		qsn11 = 0
		qsn12 = 0
		qsn13 = 0
		qsn14 = 0
		qsn15 = 0 
		qsn16 = 0 
		qsn17 = 0 
		qsn18 = 0 
		qsn19 = 0 
		qsn20 = 0 
		qsn21 = 0 
		qsn22 = 0 
		qsn23 = 0 
		qsn24 = 0 
		qsn25 = 0 

		results = None
		score = 0
		init_data = RegisterPerson.objects.get(pk=user_id)
		first_name = init_data.first_name
		last_name = init_data.last_name


		if request.method == 'POST':
			answer1 = request.POST.get('answer1')
			answer2 = request.POST.get('answer2')
			answer3 = request.POST.get('answer3')
			answer4 = request.POST.get('answer4')
			answer5 = request.POST.get('answer5')
			answer6 = request.POST.get('answer6')
			answer7 = request.POST.get('answer7')
			answer8 = request.POST.get('answer8')
			answer9 = request.POST.get('answer9')
			answer10 = request.POST.get('answer10')
			answer11= request.POST.get('answer11')
			answer12 = request.POST.get('answer12')
			answer13 = request.POST.get('answer13')
			answer14 = request.POST.get('answer14')
			answer15 = request.POST.get('answer15')
			answer16 = request.POST.get('answer16')
			answer17 = request.POST.get('answer17')
			answer18 = request.POST.get('answer18')
			answer19 = request.POST.get('answer19')
			answer20 = request.POST.get('answer20')
			answer21 = request.POST.get('answer21')
			answer22 = request.POST.get('answer22')
			answer23 = request.POST.get('answer23')
			answer24 = request.POST.get('answer24')
			answer25 = request.POST.get('answer25')

			tests =  ExamHistory.objects.get(applicant=user_id)
			tests.answer1 = answer1
			tests.answer2 = answer2
			tests.answer3 = answer3
			tests.answer4 = answer4
			tests.answer5 = answer5
			tests.answer6 = answer6
			tests.answer7 = answer7
			tests.answer8 = answer8
			tests.answer9 = answer9
			tests.answer10 = answer10
			tests.answer11 = answer11
			tests.answer12 = answer12
			tests.answer13 = answer13
			tests.answer14 = answer14
			tests.answer15 = answer15
			tests.answer16 = answer16
			tests.answer17 = answer17
			tests.answer18 = answer18
			tests.answer19 = answer19
			tests.answer20 = answer20
			tests.answer21 = answer21
			tests.answer22 = answer22
			tests.answer23 = answer23
			tests.answer24 = answer24
			tests.answer25 = answer25



			if tests.correct_answer1 == answer1:
				qsn1 = 1


			if tests.correct_answer2 == answer2:
				qsn2 = 1
			# else 0

			if tests.correct_answer3 == answer3:
				qsn3 = 1
			# else 0

			if tests.correct_answer4 == answer4:
				qsn4 = 1
			# else 0

			if tests.correct_answer5 == answer5:
				qsn5 = 1
			# else 0
			if tests.correct_answer6 == answer6:
				qsn6 = 1

			if tests.correct_answer7 == answer7:
				qsn7 = 1

			if tests.correct_answer8 == answer8:
				qsn8 = 1

			if tests.correct_answer9 == answer9:
				qsn9 = 1

			if tests.correct_answer10 == answer10:
				qsn10 = 1

			if tests.correct_answer11 == answer11:
				qsn11 = 1

			if tests.correct_answer12 == answer12:
				qsn12 = 1

			if tests.correct_answer13 == answer13:
				qsn13 = 1

			if tests.correct_answer14 == answer14:
				qsn14 = 1

			if tests.correct_answer15 == answer15:
				qsn15 = 1

			if tests.correct_answer16 == answer16:
				qsn16 = 1

			if tests.correct_answer17 == answer17:
				qsn17 = 1

			if tests.correct_answer18 == answer18:
				qsn18 = 1

			if tests.correct_answer19 == answer19:
				qsn19 = 1

			if tests.correct_answer20 == answer20:
				qsn20 = 1

			if tests.correct_answer21 == answer21:
				qsn21 = 1

			if tests.correct_answer22 == answer22:
				qsn22 = 1

			if tests.correct_answer23 == answer23:
				qsn23 = 1

			if tests.correct_answer24 == answer24:
				qsn24 = 1

			if tests.correct_answer25 == answer25:
				qsn25 = 1
			
			score = qsn1 + qsn2 + qsn3 + qsn4 + qsn5 + qsn6 + qsn7 + qsn8 + qsn9 + qsn10 + qsn11 + qsn12 + qsn13 + qsn14 + qsn15 +qsn16 + qsn17 + qsn18 + qsn19 + qsn20 + qsn21 + qsn22 + qsn23 + qsn24 + qsn25

			if score >= 13:
				tests.has_passed=True

			tests.exam_score =score
			tests.is_void = True
			tests.total_scores = score
			
			tests.save(update_fields=['answer1','answer2','answer3','answer4','answer5','answer6','answer7','answer8',
				'answer9','answer10','answer11','answer12','answer13','answer14','answer15','answer16','answer17','answer18',
				'answer19','answer20','answer21', 'answer22', 'answer23', 'answer24', 'answer25',
				'exam_score','is_void', 'has_passed'])

				
			if FinalScore.objects.filter(applicant=user_id):
				final_score = FinalScore.objects.get(applicant=user_id)
				final_score.exam_score = score
				total_scores = final_score.exam_score + final_score.interview_score
				final_score.save(update_fields=['exam_score', 'total_scores'])

			else:
				FinalScore(
						first_name =first_name,
						last_name=last_name, 
						applicant = user_id,
						exam_score = score,
						total_scores= score
						).save()
		
			msg = 'Test Completed.'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(display_score)
			return HttpResponseRedirect(redirect_url)

	except Exception, e:
		msg = 'Display test error - %s' % str(e)
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(entry_test)
		return HttpResponseRedirect(redirect_url)

		

def display_score(request):
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id
	results = None

	examined = ExamHistory.objects.filter(applicant=user_id, is_void=True)
	if examined:
		results = ExamHistory.objects.get(applicant=user_id)
	
	return render(request, 'exams/display_score.html', {'results':results})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def entry_test(request):
	# try:
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	examined = ExamHistory.objects.filter(applicant=user_id, is_void=True)
	career_profiled = CareerDetails.objects.filter(person=user_id)
	education_profiled = EducationDetails.objects.filter(person=user_id)
	applied = Applications.objects.filter(applicant=user_id)
	approved = Applications.objects.filter(applicant=user_id, is_approved=True)


	if examined:
		msg = 'You have done an entry test already .'
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(display_score)
		return HttpResponseRedirect(redirect_url)

	if not education_profiled:
		msg = 'Please fill out your education details under the student profile section first .'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(display_score)
		return HttpResponseRedirect(redirect_url)

	if not applied:
		msg = 'Please submit an application first .'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(display_score)
		return HttpResponseRedirect(redirect_url)

	if not approved:
		msg = 'Your application is pending approval or has been rejected.'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(display_score)
		return HttpResponseRedirect(redirect_url)

			
	else:
		return render(request, 'exams/entry_test.html')


def view_exams(request):
	exams = ExamHistory.objects.all()

	msg = 'Showing results '
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'exams/view_exams.html',
				  {'exams':exams })