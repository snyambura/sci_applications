from django.contrib import admin
import csv
import time
from django.http import HttpResponse
from .models import QuestionBank
from main.admin import dump_to_csv, export_xls, export_xlsx

# Register your models here.
class ExaminationAdmin(admin.ModelAdmin):

    search_fields = ['course']
    list_display = ['course', 'question','choice_a','choice_b','choice_c','choice_d','correct_answer']
    list_filter = ['course']
    actions = [dump_to_csv, export_xls, export_xlsx]

admin.site.register(QuestionBank, ExaminationAdmin)