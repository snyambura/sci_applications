"""Main configurations information."""
from django.apps import AppConfig


class AuthAppConfig(AppConfig):
    """Details for authentication module."""

    name = 'authentication'
    verbose_name = 'User Management'


class MainAppConfig(AppConfig):
    """Details for common application module."""

    name = 'main'
    verbose_name = 'System Lookups and Settings'


class RegAppConfig(AppConfig):
    """Details for Registry modules."""

    name = 'registry'
    verbose_name = 'Registry Management'


class AccessAppConfig(AppConfig):
    """Details for Registry modules."""

    name = 'access'
    verbose_name = 'Access Management'
