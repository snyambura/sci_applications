
from datetime import datetime, timedelta
from django.shortcuts import render
from registry.functions import dashboard
from authentication.models import AppUser
from authentication.forms import RegistrationForm


def home(request):
    """Some default page for the home page / Dashboard."""
    my_dates = []
    templ = 'home.html'
    form = RegistrationForm()
    try:
        dash = dashboard()
        start_date = datetime.now()
        summary = {}
        # summary['applicants'] = '{:,}'.format(dash['applicants'])
        # summary['entry_tests'] = '{:,}'.format(dash['entry_tests'])
        # # summary['interviews'] = '{:,}'.format(dash['interviews'])
        # # summary['applications_approved'] = '{:,}'.format(dash['applications_approved'])
        # # summary['passed_tests'] = '{:,}'.format(dash['passed_tests'])
        # # summary['pending_evaluation'] = '{:,}'.format(dash['pending_evaluation'])
        for date in range(0, 21, 2):
            end_date = start_date + timedelta(days=date)
            show_date = datetime.strftime(end_date, "%d-%b-%y")
            final_date = str(show_date).replace(' ', '&nbsp;')
            my_dates.append("[%s, '%s']" % (date, final_date))
        dates = ','.join(my_dates)
        if request.user.is_authenticated():
            if request.user.is_staff:
                templ= 'dashboard.html'
            else:
                templ= 'applicant_home.html' 
        return render(request, templ,
                  {'status': 200, 'dates': dates, 'form': form})
    except Exception, e:
        raise e


def handler_400(request):
    """Some default page for Bad request error page."""
    try:
        return render(request, '400.html', {'status': 400})
    except Exception, e:
        raise e


def handler_404(request):
    """Some default page for the Page not Found."""
    try:
        return render(request, '404.html', {'status': 404})
    except Exception, e:
        raise e


def handler_500(request):
    """Some default page for Server Errors."""
    try:
        return render(request, '500.html', {'status': 500})
    except Exception, e:
        raise e
