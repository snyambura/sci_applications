"""
URL Configuration.

Other urls are import
Put here only urls not specific to app
"""
from django.conf.urls import include, url
from django.contrib import admin
from authentication import urls as authentication_urls
from registry import urls as registry_urls
from api import urls as api_urls
from biodata import urls as biodata_urls
from exams import urls as exams_urls
from interview import urls as interview_urls
from application import urls as application_urls
from reports import urls as reports_urls
from django.contrib.auth.views import (
    password_reset_done, password_change, password_change_done)
from authentication.views import password_reset
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'sci_applications.views.home', name='home'),
    url(r'^login/$', 'authentication.views.log_in', name='login'),
    url(r'^logout/$', 'authentication.views.log_out', name='logout'),
    url(r'^register/$', 'authentication.views.register', name='register'),
    url(r'^authentication/', include(authentication_urls)),
    url(r'^registry/', include(registry_urls)),
    url(r'^exams/', include(exams_urls)),
    url(r'^api/', include(api_urls)),
    url(r'^biodata/', include(biodata_urls)),
    url(r'^interview/', include(interview_urls)),
    url(r'^reports/', include(reports_urls)),
    url(r'^application/', include(application_urls)),
    url(r'^$', RedirectView.as_view(url='/biodata/doc_upload/', permanent=True)),
    url(r'^accounts/login/$', 'authentication.views.log_in', name='login'),
    url(r'^accounts/password/reset/$', password_reset,
        {'template_name': 'registration/password_reset.html'},
        name='password_reset'),
    url(r'^accounts/password/reset/done/$', password_reset_done,
        {'template_name': 'registration/password_reset_done.html'},
        name='password_reset_done'),
    url(r'^accounts/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        'authentication.views.reset_confirm', name='password_reset_confirm'),
    url(r'^reset/$', 'authentication.views.reset', name='reset'),
    url(r'^accounts/password/change/$', password_change,
        {'post_change_redirect': '/accounts/password/change/done/',
         'template_name': 'registration/password_change.html'},
        name='password_change'),
    url(r'^accounts/password/change/done/$', password_change_done,
        {'template_name': 'registration/password_change_done.html'}),
    url(r'^mpesa/', include('mpesapy.urls',namespace='mpesa')),
    url(r'^$', RedirectView.as_view(url='/biodata/doc_upload/', permanent=True)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}
  ),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt',
                                               content_type='text/plain'))
    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler400 = 'sci_applications.views.handler_400'
handler404 = 'sci_applications.views.handler_404'
handler500 = 'sci_applications.views.handler_500'

admin.site.site_header = 'SCI MSC Administration'
admin.site.site_title = 'SCI MSC administration'
admin.site.index_title = 'SCI MSC admin'
