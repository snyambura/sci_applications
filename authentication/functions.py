"""Common functions for authentication module."""
from django.utils import timezone

from .models import CPOVCRole
 # CPOVCUserRoleGeoOrg
from main.models import RegTemp


def get_allowed_units_county(user_id):
    """
    Return dict with list of allowed group ids mapped to org units.

    and for sub counties do the reverse just list of sub-counties
    """
    try:
        geo_orgs = get_group_geos_org(user_id)
        ex_areas, ex_orgs = [], {}
        for geo_org in geo_orgs:
            if geo_org['area_id']:
                ex_areas.append(geo_org['area_id'])
            if geo_org['org_unit_id']:
                if geo_org['org_unit_id'] in ex_orgs:
                    ex_orgs[geo_org['org_unit_id']].append(geo_org['group_id'])
                else:
                    ex_orgs[geo_org['org_unit_id']] = [geo_org['group_id']]
    except Exception, e:
        error = 'Error getting persons orgs/sub-county groups - %s' % (str(e))
        print error
    else:
        return ex_areas, ex_orgs


def get_groups(grp_prefix='group_'):
    """Return list of ids and CPIMS codes."""
    groups = {}
    disallowed_group = [11]
    try:
        results = CPOVCRole.objects.filter().values(
            'group_ptr_id', 'group_id', 'group_name')
        for group in results:
            group_id = '%s%s' % (grp_prefix, str(group['group_id']))
            if group_id not in disallowed_group:
                groups[group['group_ptr_id']] = group_id

    except Exception, e:
        error = 'Error getting groups - %s' % (str(e))
        print error
    else:
        return groups

def save_temp_data(user_id, page_id, page_data):
    """"Method to save temp form data for this person and page."""
    try:
        new_tmp, ctd = RegTemp.objects.update_or_create(
            user_id=user_id, page_id=page_id,
            defaults={'data': str(page_data), 'created_at': timezone.now(),
                      'user_id': user_id, 'page_id': page_id},)
    except Exception, e:
        print 'save tmp error - %s' % (str(e))
        pass

