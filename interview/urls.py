from django.conf.urls import patterns, url


urlpatterns = patterns(

    'interview.views',
    url(r'^interview_assessment/$', 'interview_assessment', name='interview_assessment'),
    url(r'^view_interviews/$', 'view_interviews', name='view_interviews'),
    url(r'^view_scores/$', 'view_scores', name='view_scores'),
    url(r'^interview_score/(?P<id>\d+)/$','interview_score', name='interview_score'),
    url(r'^interview/(?P<id>\d+)/$','interview', name='interview'),
    url(r'^consolidated_interview/(?P<id>\d+)/$','consolidated_interview', name='consolidated_interview'),

    )
    