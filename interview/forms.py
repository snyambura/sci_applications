from django import forms
from django.utils.translation import ugettext_lazy as _
from main.functions import get_list

course_list=get_list('masters_program_id', 'Please Select')
choices_list=get_list('choice_id', 'Please Select')
psearch_criteria_list = get_list('psearch_criteria_type_id', 'Select Criteria')
discipline_list = get_list('discipline_id', 'Please Select')



class InterviewForm(forms.Form):
		applicant = forms.CharField(widget=forms.TextInput(
	        attrs={'class': 'form-control',
	               'id': 'applicant',
	               'type': 'hidden'
	               #'data-parsley-required': "true",
	               #'data-parsley-group': 'group0'
	               }))
		comment1 = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _('Comments '),
							 'class': 'form-control',
							 'id': 'comment1',
							 'rows': '2',
							 'data-parsely-group': "group0",
							 }))
		score1 = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Score '),
							 'class': 'form-control',
							 'id': 'score1',
							 'data-parsely-group': "group0",
							 'data-parsley-required': "true"
							 }))
		comment2 = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _('Comments '),
							 'class': 'form-control',
							 'id': 'comment2',
							 'rows': '2',
							 'data-parsely-group': "group1"
							 }))
		score2 = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Score '),
							 'class': 'form-control',
							 'id': 'score2',
							 'data-parsely-group': "group1",
							 'data-parsley-required': "true"
							 }))
		comment3 = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _(' Comments'),
							 'class': 'form-control',
							 'id': 'comment3',
							 'rows': '2',
							 'data-parsely-group': "group2"
							 }))
		score3 = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Score '),
							 'class': 'form-control',
							 'id': 'score3',
							 'data-parsely-group': "group2",
							 'data-parsley-required': "true"
							 }))
		comment4 = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _('Comments'),
							 'class': 'form-control',
							 'id': 'comment4',
							 'rows': '2',
							 'data-parsely-group': "group3"
							 }))
		score4 = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Score '),
							 'class': 'form-control',
							 'id': 'score4',
							 'data-parsely-group': "group3",
							 'data-parsley-required': "true",
							 }))
		

class SearchForm(forms.Form):
    search_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Enter Applicant\'s Name(s)'),
               'class': 'form-control',
               'id': 'search_name',
               'data-parsley-group': 'primary_',
               'data-parsley-required': 'true'}))

    search_criteria = forms.ChoiceField(choices=psearch_criteria_list,
                                        initial='0',
                                        required=True,
                                        widget=forms.Select(
                                            attrs={'class': 'form-control',
                                                   'id': 'search_criteria',
                                                   # 'readonly':'true',
                                                   'data-parsley-required': 'true'})
                                        )

    # form_type_search = forms.ChoiceField(choices=form_type_list,
    #                                      initial='0',
    #                                      required=True,
    #                                      widget=forms.Select(
    #                                          attrs={'class': 'form-control',
    #                                                 'id': 'form_type_search',
    #                                                 'data-parsley-required': 'true'})
    #                                      )