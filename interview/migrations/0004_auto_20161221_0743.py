# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0003_auto_20161220_1648'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consolidatedscore',
            name='applicant',
            field=models.IntegerField(default=404, null=True),
        ),
        migrations.AlterField(
            model_name='interviewhistory',
            name='applicant',
            field=models.IntegerField(default=404, null=True),
        ),
    ]
