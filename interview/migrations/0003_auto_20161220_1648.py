# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0002_auto_20161220_1635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consolidatedscore',
            name='applicant',
            field=models.ForeignKey(default=1, to='biodata.RegisterPerson'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='interviewhistory',
            name='applicant',
            field=models.ForeignKey(default=1, to='biodata.RegisterPerson'),
            preserve_default=False,
        ),
    ]
