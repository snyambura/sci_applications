# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ACInterviewQuestions',
        ),
        migrations.DeleteModel(
            name='CIInterviewQuestions',
        ),
        migrations.DeleteModel(
            name='DCTInterviewQuestions',
        ),
        migrations.DeleteModel(
            name='ITMInterviewQuestions',
        ),
    ]
