from __future__ import division
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib import messages
from django.utils import timezone
from django.core import serializers
from django.conf import settings
from django.db.models import Q
import json
import operator
import random
import uuid
from datetime import datetime
from sci_applications.views import home 
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from main.functions import ( new_guid_32, get_dict, convert_date, get_list_of_persons, rank_results)
from interview.forms import ( SearchForm, InterviewForm)
from authentication.models import AppUser
from biodata.models import RegisterPerson
from exams.models import FinalScore, ExamHistory
from application.models import Applications
from interview.models import (InterviewHistory, ConsolidatedScore)

# Create your views here.


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def interview(request, id):

	# Get Init Data
	init_data = RegisterPerson.objects.filter(pk=id)
	applicant=id
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	try:
		panelists = InterviewHistory.objects.filter(panelist=user_id, applicant=id)
		if panelists:
			msg = 'You have already submitted your score for this applicant.'
			messages.add_message(request, messages.ERROR, msg)
			redirect_url = reverse(interview_score, kwargs={'id':id})
			return HttpResponseRedirect(redirect_url)

		if request.method == 'POST':
			# Get app_user
			username = request.user.get_username()
			app_user = AppUser.objects.get(username=username)
			user_id = app_user.id
			interview_id= new_guid_32()

			comment1 = request.POST.get('comment1')
			score1 = request.POST.get('score1')
			comment2 = request.POST.get('comment2')
			score2 = request.POST.get('score2')
			comment3 = request.POST.get('comment3')
			score3 = request.POST.get('score3')
			comment4 = request.POST.get('comment4')
			score4 = request.POST.get('score4')

			total_score = int(score1) + int(score2) + int(score3) + int(score4)
			

			InterviewHistory(
				interview_id=interview_id,
				comment1=comment1,
				score1=score1,
				comment2=comment2,
				score2=score2,
				comment3=comment3,
				score3=score3,
				comment4=comment4,
				score4=score4,
				panelist=user_id,
				applicant=applicant,
				interview_score=total_score
				
			).save()


			msg = 'Interview Completed'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(interview_score , kwargs={'id':id})
			return HttpResponseRedirect(redirect_url)
			
	except Exception, e:
		print 'An error occured while saving - %s' % str(e)
		msg = 'Error saving Question.'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(interview_assessment)
		return HttpResponseRedirect(redirect_url)

	form = InterviewForm({'applicant': id})
	return render(request,
				  'interview/interview.html',
				  {'form': form,
				   'init_data': init_data})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def interview_assessment(request):
	resultsets = Applications.objects.filter(is_approved=True)
	if resultsets:

		check_fields = ['masters_program_id']
		vals = get_dict(field_name=check_fields)
		
		   
		msg = 'Showing approved applicants' 
		messages.add_message(request, messages.INFO, msg)
		return render(request, 'interview/interview_assessment.html',
					  {'resultsets': resultsets,'vals': vals  })
	else:
		msg = 'No applicants to be interviewed.'
		messages.add_message(request, messages.ERROR, msg)
		return render(request, 'interview/interview_assessment.html')
	# except Exception, e:
	# 	msg = ' error - %s' % (str(e))
	# 	messages.add_message(request, messages.ERROR, msg)
	# return HttpResponseRedirect(reverse(interview_assessment))



def interview_score(request , id):

	init_data = RegisterPerson.objects.filter(pk=id)
	applicant=id
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	panelist_data = RegisterPerson.objects.filter(pk=user_id)
	applications = Applications.objects.filter(applicant=applicant)
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)

	score1 = 0
	score2 = 0
	score3 = 0
	score4 = 0
	# results = None
	total_score = 0

	interviewed = InterviewHistory.objects.filter(applicant=id, panelist=user_id)
	if interviewed:
		interviews = InterviewHistory.objects.get(applicant=id, panelist=user_id)

		assessment_id = interviews.interview_id

		
		msg = 'Showing results '
		messages.add_message(request, messages.INFO, msg)
		return render(request, 'interview/interview_score.html',
					  {'vals': vals,
					   'interviews':interviews,
					   'applications': applications,
					   'total_score':total_score,
					   'init_data':init_data,
					   'panelist_data': panelist_data
						   })


def consolidated_interview(request , id):

	total_score1 =0
	total_score2 = 0
	total_score3 = 0
	total_score4 = 0
	average_score1 = 0
	average_score2 = 0 
	average_score3 = 0
	average_score4 = 0

	init_data = RegisterPerson.objects.filter(pk=id)
	applicant=id
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id
	interviews = InterviewHistory.objects.filter(applicant=id)
	panelist_data = RegisterPerson.objects.filter(pk=user_id)
	applications = Applications.objects.filter(applicant=applicant)
	for appplctn in applications:
		first_name = appplctn.first_name
		last_name = appplctn.last_name
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	count=interviews.count()
	total_score = 0

	pool= list( interviews)
	results = pool[:count]

	scoresa = results[0]
	score1 = scoresa.interview_score
	if count >= 2:
		scoresb = results[1]
		score2 = scoresb.interview_score
	else:
		score2 = 0
	if count >= 3:
		scoresc = results[2]
		score3 = scoresc.interview_score
	else:
		score3 = 0
	if count >= 4:
		scoresd = results[3]
		score4 = scoresd.interview_score
	else:
		score4 = 0

	for interview in interviews:
		scores = interview.interview_score
		total_score = scores + total_score

		
	average_score = total_score/count
	

	if ConsolidatedScore.objects.filter(applicant=id):
		consolidated = ConsolidatedScore.objects.get(applicant=id)
		consolidated.score1 =score1 
		consolidated.score2 =score2 
		consolidated.score3 =score3 
		consolidated.score4 =score4 
		consolidated.total_score =total_score 
		consolidated.average_score =average_score
		consolidated.save(update_fields=['score1', 'score2','score3', 'score4', 'total_score', 'average_score'])

		
	else:
		ConsolidatedScore(
						score1 =score1, 
						score2 =score2, 
						score3 =score3, 
						score4 =score4, 
						total_score =total_score, 
						average_score =average_score, 
						applicant = id,
						first_name= first_name,
						last_name=last_name
						).save()

	if FinalScore.objects.filter(applicant=id):
		final_score = FinalScore.objects.get(applicant=id)
		both_scores = average_score + final_score.exam_score
		final_score.total_scores = both_scores
		final_score.interview_score = average_score
		final_score.save(update_fields=['interview_score', 'total_scores'])

	else:
		FinalScore(
					first_name =first_name,
					last_name=last_name, 
					applicant = id,
					interview_score = average_score,
					total_scores = average_score
					).save()



	msg = 'Showing results '
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'interview/consolidated_interview.html',
				  {'vals': vals,
				   'interviews':interviews,
				   'score1':score1,
				   'score2':score2,
				   'score3':score3,
				   'score4':score4,
				   'applications': applications,
				   'total_score':total_score,
				   'average_score':average_score,
				   'init_data':init_data,
				   'panelist_data': panelist_data
					   })


def view_interviews(request):
	interviews = ConsolidatedScore.objects.filter(is_void=False)

	msg = 'Showing results '
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'interview/view_interviews.html',
				  {'interviews':interviews })


def view_scores(request):
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	interviewed = ConsolidatedScore.objects.filter(applicant=user_id)
	# if interviewed:
	# 	interviews = ConsolidatedScore.objects.get(applicant=user_id)

	msg = 'Showing results '
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'interview/view_scores.html',
				  {'interviewed':interviewed })



