from datetime import datetime, date
from difflib import SequenceMatcher
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.conf import settings
from django.dispatch import receiver
import uuid
from authentication.models import AppUser
from biodata.models import RegisterPerson

# Create your models here.

class InterviewHistory(models.Model):
	interview_id = models.UUIDField(
		primary_key=True, default=uuid.uuid1, editable=False)
	panelist = models.CharField(max_length=4, default=None)
	score1 = models.IntegerField(default=0)
	comment1 = models.CharField(max_length=10000, default=None)
	score2 = models.IntegerField(default=0)
	comment2 = models.CharField(max_length=10000, default=None)
	score3 = models.IntegerField(default=0)
	comment3 = models.CharField(max_length=10000, default=None)
	score4 = models.IntegerField( default=0)
	comment4 = models.CharField(max_length=10000, default=None)
	interview_score = models.IntegerField( default=0)
	applicant = models.IntegerField(null=True, default=404)
	timestamp_submitted = models. DateTimeField(default=timezone.now)
	is_void = models.BooleanField(default=False)

	class Meta:
		db_table = 'interview_history'


class ConsolidatedScore(models.Model):
	score1 = models.IntegerField(default=0)
	score2 = models.IntegerField(default=0)
	score3 = models.IntegerField(default=0)
	score4 = models.IntegerField(default=0)
	total_score = models.IntegerField(default = 0)
	average_score = models.IntegerField(default = 0)
	applicant = models.IntegerField(null=True, default=404)
	first_name = models.CharField(max_length=5000, null=True)
	last_name = models.CharField(max_length=5000, null=True)
	timestamp_updated = models. DateTimeField(default=timezone.now)
	is_void = models.BooleanField(default=False)

	class Meta:
		db_table = 'consolidated_score'
