# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0002_auto_20161205_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applications',
            name='applicant',
            field=models.ForeignKey(default=404, to=settings.AUTH_USER_MODEL),
        ),
    ]
