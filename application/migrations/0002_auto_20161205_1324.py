# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='applications',
            name='amount_paid',
            field=models.CharField(max_length=5000, null=True),
        ),
        migrations.AddField(
            model_name='applications',
            name='sender_phone',
            field=models.CharField(max_length=5000, null=True),
        ),
        migrations.AddField(
            model_name='applications',
            name='transaction_reference',
            field=models.CharField(max_length=5000, null=True),
        ),
    ]
