# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0003_auto_20161209_0925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applications',
            name='applicant',
            field=models.IntegerField(default=404, null=True),
        ),
    ]
