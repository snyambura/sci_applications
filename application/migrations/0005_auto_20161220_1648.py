# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0004_auto_20161209_0928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applications',
            name='applicant',
            field=models.ForeignKey(default=1, to='biodata.RegisterPerson'),
            preserve_default=False,
        ),
    ]
