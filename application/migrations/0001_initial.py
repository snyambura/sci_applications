# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Applications',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course', models.CharField(max_length=5, null=True)),
                ('session', models.CharField(max_length=5, null=True)),
                ('discipline', models.CharField(max_length=5, null=True)),
                ('first_name', models.CharField(max_length=5000, null=True)),
                ('last_name', models.CharField(max_length=5000, null=True)),
                ('payment_status', models.BooleanField(default=False)),
                ('timestamp_applied', models.DateTimeField(default=django.utils.timezone.now)),
                ('applicant', models.IntegerField(default=404, null=True)),
                ('is_approved', models.BooleanField(default=False)),
                ('is_void', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'applications',
            },
        ),
    ]
