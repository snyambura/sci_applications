# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0006_auto_20161220_1649'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='applications',
            name='amount_paid',
        ),
        migrations.RemoveField(
            model_name='applications',
            name='payment_status',
        ),
        migrations.RemoveField(
            model_name='applications',
            name='sender_phone',
        ),
        migrations.RemoveField(
            model_name='applications',
            name='transaction_reference',
        ),
    ]
