from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.contrib import messages
from django.utils import timezone
from django.core import serializers
from django.conf import settings
from django.db.models import Q
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
import operator
import random
import uuid
from datetime import datetime
from sci_applications.views import home 
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from main.functions import ( new_guid_32, get_dict, convert_date, get_list_of_persons, rank_results)
from application.forms import (ApplicationForm)
from application.models import Applications
from biodata.models import RegisterPerson, EducationDetails, CareerDetails, Document
from authentication.models import AppUser
from AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException




def make_application(request):
	return render(request, 'application/make_application.html')


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def start_application(request):
	# try:
	username = request.user.get_username()
	app_user = AppUser.objects.get(username=username)
	user_id = app_user.id

	applicant_education = EducationDetails.objects.filter(person=user_id)
	applicant_career = CareerDetails.objects.filter(person=user_id)
	applicants = Applications.objects.filter(applicant=user_id)
	document = Document.objects.filter(person=user_id)
	
	if (not applicant_education) :
		msg = 'Please fill out your education details under the student profile section first .'
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(make_application)
		return HttpResponseRedirect(redirect_url)

	if (not document) :
		msg = 'Please attach supporting documents .'
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(make_application)
		return HttpResponseRedirect(redirect_url)

	if applicants:
		msg = 'Application already submitted .'
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(make_application)
		return HttpResponseRedirect(redirect_url)

			
	else:
		return render(request, 'application/start_application.html')


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def application(request): 
	try:
		username = request.user.get_username()
		app_user = AppUser.objects.get(username=username)
		user_id = app_user.id

		if request.method == 'POST':
			course = request.POST.get('course')
			session = request.POST.get('session')
			discipline= request.POST.get('discipline')

			person = RegisterPerson.objects.get(pk=user_id)
			first_name = person.first_name
			last_name = person.last_name

			

			Applications(
				first_name=first_name,
				last_name=last_name,
				course=course,
				session=session,
				discipline=discipline,
				applicant=user_id,
				).save()
			

			msg = 'Application submitted. You will receive a notification for further instructions.'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(start_application)
			return HttpResponseRedirect(redirect_url)

	except Exception, e:
		print 'An error occured while saving - %s' % str(e)
		msg = 'Application not successful! Please ensure that you have filled out the student profile section. Also, be ware that you can only apply once'
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(application)
		return HttpResponseRedirect(redirect_url)
	form = ApplicationForm()
	return render(request, 'application/application.html', {'form': form})


@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_applications(request):
	applicants = Applications.objects.filter(is_void=False) 

	msg = 'Showing unreviewed applications'
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'application/view_applications.html',
				  {'applicants': applicants,
				  'vals': vals
				   })

@login_required
def view_reviewed_applications(request):
	applicants = Applications.objects.filter(is_void=True) 

	msg = 'Showing reviewed applications'
	check_fields = ['masters_program_id']
	vals = get_dict(field_name=check_fields)
	messages.add_message(request, messages.INFO, msg)
	return render(request, 'application/view_reviewed_applications.html',
				  {'applicants': applicants,
				  'vals': vals
				   })


@login_required
def view_applicant(request, id):
	person = 0
	try:
		init_data = RegisterPerson.objects.filter(pk=id)
		applicant = id

		aplcnt= Applications.objects.get(applicant=applicant)
		person_education = EducationDetails.objects.filter(person=applicant, is_void=False)
		person_career = CareerDetails.objects.filter(person=applicant, is_void=False)
		documents = Document.objects.filter(person=applicant)

		check_fields = ['masters_program_id', 'education_level_id', 'award_id',
						'sex_id', 'university_id','doctype_id']
		vals = get_dict(field_name=check_fields)


		return render(request,
					  'application/view_applicant.html',
					  {'init_data': init_data, 
					  'person':person,
					  'person_education':person_education,
					  'person_career':person_career,
					  'documents':documents,
					  'aplcnt': aplcnt,
					  'vals': vals})


	except Exception, e:
		# raise e
		msg = 'Persons error - %s' % (str(e))
		messages.add_message(request, messages.ERROR, msg)
		return HttpResponseRedirect(reverse(view_applications)) 


def approve_applicant(request, id):
	try:
		applicant = id
		applicant_email=RegisterPerson.objects.get(pk=id)
		record = Applications.objects.get(applicant=applicant, is_void=False, is_approved=False)
		record.is_void = True
		record.is_approved = True
		record.save(
			update_fields=['is_void', 'is_approved'])
		

		send_mail('Application Status', 'Your application to the School of Informatics, University of Nairobi has been approved, please proceed to sit the entry test before coming in for an oral interview in 2 days.', 'scimscapplications@gmail.com',
		    [applicant_email.email], fail_silently=False)
		recipient =[]

		msg = 'Approval Successful'
		username = "snyambura"
		apikey   = "2bd255e9d1d3cead5cf36b0418ac38bc5a2edd5676ea8e681e432a99c62930f9"

		phone_no = str(applicant_email.phone_no)
		to = phone_no

		message = "Your application to the School of Informatics, University of Nairobi has been approved, please proceed to sit the entry test before coming in for an oral interview in 2 days"
		gateway = AfricasTalkingGateway(username, apikey)

		gateway.sendMessage(to, message)

		for recipient in results:
			# status is either "Success" or "error message"
			print 'number=%s;status=%s;messageId=%s;cost=%s' % (recipient['number'],
																recipient['status'],
																recipient['messageId'],
																recipient['cost'])

		msg =  'Application notification succesfully sent' 
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(view_applications)
		return HttpResponseRedirect(redirect_url)

	except AfricasTalkingGatewayException, e:
		msg =  'Encountered an error while sending: %s' % str(e)
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(view_applications)
		return HttpResponseRedirect(redirect_url)
		# return render(request, 'application/view_applications.html')

def disapprove_applicant(request, id):
	try:
		applicant = id
		applicant_email=RegisterPerson.objects.get(pk=id)
		record = Applications.objects.get(applicant=applicant, is_void=False)
		record.is_void = True
		record.save(
			update_fields=['is_void'])

		send_mail('Application Status', 'Your MSC application at the School of Informatics, University of Nairobi was unsuccessful.', 'scimscapplications@gmail.com',
		    [applicant_email.email], fail_silently=False)
		recipient = []

		msg = 'Approval Unsuccessful'
		username = "snyambura"
		apikey   = "2bd255e9d1d3cead5cf36b0418ac38bc5a2edd5676ea8e681e432a99c62930f9"
		# Specify the numbers that you want to send to in a comma-separated list
		# Please ensure you include the country code (+254 for Kenya)
		# to      = "+254710297531"
		phone_no = str(applicant_email.phone_no)
		to = phone_no
		# to = applicant_email.phone_no
		# And of course we want our recipients to know what we really do
		message = "Your MSC application at the School of Informatics, University of Nairobi was unsuccessful."
		# Create a new instance of our awesome gateway class
		gateway = AfricasTalkingGateway(username, apikey)

		results = gateway.sendMessage(to, message)
		for recipient in results:
			# status is either "Success" or "error message"
			print 'number=%s;status=%s;messageId=%s;cost=%s' % (recipient['number'],
																recipient['status'],
																recipient['messageId'],
																recipient['cost'])

		msg =  'Application notification succesfully sent' 
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(view_applications)
		return HttpResponseRedirect(redirect_url)

	except AfricasTalkingGatewayException, e:
		msg =  'Encountered an error while sending notification: %s' % str(e)
		messages.add_message(request, messages.INFO, msg)
		redirect_url = reverse(view_applications)
		return HttpResponseRedirect(redirect_url)


