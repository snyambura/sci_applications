from django.conf.urls import patterns, url


urlpatterns = patterns(

    'application.views',
    url(r'^application/$', 'application', name='application'),
    # url(r'^mpesa/$', 'application', name='application'),
    url(r'^make_application/$', 'make_application', name='make_application'),
    url(r'^start_application/$', 'start_application', name='start_application'),
    url(r'^view_applications/$', 'view_applications', name='view_applications'),
    url(r'^view_reviewed_applications/$', 'view_reviewed_applications', name='view_reviewed_applications'),
    url(r'^view_applicant/(?P<id>\d+)/$','view_applicant', name='view_applicant'),
    url(r'^approve_applicant/(?P<id>\d+)/$','approve_applicant', name='approve_applicant'),
    url(r'^disapprove_applicant/(?P<id>\d+)/$','disapprove_applicant', name='disapprove_applicant'),

    )