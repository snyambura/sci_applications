from datetime import datetime, date
from difflib import SequenceMatcher
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.conf import settings
from django.dispatch import receiver
import uuid
from authentication.models import AppUser

# Create your models here.
class Applications(models.Model):
	course = models.CharField(max_length=5, null=True)
	session = models.CharField(max_length=5, null=True)
	discipline = models.CharField(max_length=5, null=True)
	first_name = models.CharField(max_length=5000, null=True)
	last_name = models.CharField(max_length=5000, null=True)
	# payment_status=models.BooleanField(default=False)
	timestamp_applied = models.DateTimeField(default=timezone.now)
	applicant = models.IntegerField(null=True, default=404)
	is_approved=models.BooleanField(default=False)
	is_void=models.BooleanField(default=False)

	class Meta:
		db_table = 'applications'
