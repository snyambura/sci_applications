from django import forms
from django.utils.translation import ugettext_lazy as _
from main.functions import get_list

course_list = get_list('masters_program_id', 'Please Select')
intake_list = get_list('intake_id', 'Please Select')
choices_list = get_list('choice_id', 'Please Select')
discipline_list = get_list('discipline_id', 'Please Select')

class ApplicationForm(forms.Form):
	 

		course = forms.ChoiceField(choices=course_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'course',
												'data-parsley-required': "true",
							 					'data-parsley-group': 'group0'
												}))
		discipline = forms.ChoiceField(choices=discipline_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Appropriate Course'),
												'class': 'form-control',
												'id': 'discipline',
												'data-parsley-required': "true",
							 					'data-parsley-group': 'group0'
												}))

		session = forms.ChoiceField(choices=intake_list,
								 initial='0',
								 widget=forms.Select(
								 attrs={'placeholder': _('Preferred Intake'),
												'class': 'form-control',
												'id': 'session',
												'data-parsley-required': "true",
							 					'data-parsley-group': 'group0'
												}))
		mpesa_code = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('MPESA Transaction Reference'),
							 'class': 'form-control',
							 'id': 'mpesa_code',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))
		mpesa_sender = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Payer\'s Phone Number   '),
							 'class': 'form-control',
							 'id': 'mpesa_sender',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))
		mpesa_amt = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Amount Paid  '),
							 'class': 'form-control',
							 'id': 'mpesa_amt',
							 'data-parsley-required': "false",
							 'data-parsley-group': 'group2'
							 }))