# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RegPersonsAuditTrail',
            fields=[
                ('transaction_id', models.AutoField(serialize=False, primary_key=True)),
                ('transaction_type_id', models.CharField(max_length=4, null=True, db_index=True)),
                ('interface_id', models.CharField(max_length=4, null=True, db_index=True)),
                ('timestamp_modified', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('meta_data', models.TextField(null=True)),
                ('app_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'reg_persons_audit_trail',
                'verbose_name': 'Persons Audit Trail',
                'verbose_name_plural': 'Persons Audit Trails',
            },
        ),
    ]
