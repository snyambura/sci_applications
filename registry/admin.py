"""Admin backend for editing some admin details."""
from django.contrib import admin

from .models import (RegPersonsAuditTrail)
from biodata.models import RegisterPerson


class RegPersonAdmin(admin.ModelAdmin):
    """Register persons admin."""

    search_fields = ['first_name', 'last_name']
    list_display = ['id', 'first_name', 'last_name', 
                      'is_void']
    # readonly_fields = ['id']
    list_filter = ['is_void']

admin.site.register(RegisterPerson, RegPersonAdmin)


# class RegOrgUnitAdmin(admin.ModelAdmin):
#     """Register persons admin."""

#     search_fields = ['org_unit_name', 'org_unit_id_vis']
#     list_display = ['id', 'org_unit_id_vis', 'org_unit_name',
#                     'parent_org_unit_id', 'is_void']
#     # readonly_fields = ['id']
#     list_filter = ['is_void', 'org_unit_type_id']

# admin.site.register(RegOrgUnit, RegOrgUnitAdmin)


# class OrgUnitAuditAdmin(admin.ModelAdmin):
#     """Register persons admin."""

#     search_fields = ['org_unit_id']
#     list_display = ['transaction_id', 'transaction_type_id', 'ip_address',
#                     'app_user_id', 'timestamp_modified']
#     # readonly_fields = ['id']
#     list_filter = ['transaction_type_id', 'app_user_id']

# admin.site.register(RegOrgUnitsAuditTrail, OrgUnitAuditAdmin)


# class PersonsAuditAdmin(admin.ModelAdmin):
#     """Register persons admin."""

#     search_fields = ['person_id']
#     list_display = ['transaction_id', 'transaction_type_id', 'ip_address',
#                     'app_user_id', 'timestamp_modified']
#     # readonly_fields = ['id']
#     list_filter = ['transaction_type_id', 'app_user_id']

# admin.site.register(RegPersonsAuditTrail, PersonsAuditAdmin)
