# """For admin view."""
# from django.contrib import admin
# from django.http import HttpResponseRedirect
# from django.core.urlresolvers import reverse
# from django.contrib.auth import REDIRECT_FIELD_NAME
# from django.contrib import messages

# from access.models import AccessLog
# from access.models import AccessAttempt


# def unlock_user(modeladmin, request, queryset):
#     """
#     These takes in a Django queryset and spits out a CSV file.

#     Generic method for any queryset
#     """
#     # model = qs.model
#     queryset.update(failures_since_start=0)
#     message = ('User(s) failed login counts reset to 0. '
#                'User(s) can now log in.')
#     messages.info(request, message)

# unlock_user.short_description = u"Unlock selected user(s)"


# class AccessAttemptAdmin(admin.ModelAdmin):
#     """Class for handling attempts."""

#     list_display = (
#         'attempt_time',
#         'ip_address',
#         'user_agent',
#         'username',
#         'path_info',
#         'failures_since_start',
#     )

#     list_filter = [
#         'attempt_time',
#         'ip_address',
#         'username',
#         'path_info',
#     ]

#     search_fields = [
#         'ip_address',
#         'username',
#         'user_agent',
#         'path_info',
#     ]

#     date_hierarchy = 'attempt_time'

#     fieldsets = (
#         (None, {
#             'fields': ('path_info', 'failures_since_start')
#         }),
#         ('Form Data', {
#             'fields': ('get_data', 'post_data')
#         }),
#         ('Meta Data', {
#             'fields': ('user_agent', 'ip_address', 'http_accept')
#         })
#     )

#     actions = [unlock_user]

# admin.site.register(AccessAttempt, AccessAttemptAdmin)


# class AccessLogAdmin(admin.ModelAdmin):
#     """Class for handling access logs."""

#     list_display = (
#         'attempt_time',
#         'logout_time',
#         'ip_address',
#         'username',
#         'user_agent',
#         'path_info',
#     )

#     list_filter = [
#         'attempt_time',
#         'logout_time',
#         'ip_address',
#         'username',
#         'path_info',
#     ]

#     search_fields = [
#         'ip_address',
#         'user_agent',
#         'username',
#         'path_info',
#     ]

#     date_hierarchy = 'attempt_time'

#     fieldsets = (
#         (None, {
#             'fields': ('path_info',)
#         }),
#         ('Meta Data', {
#             'fields': ('user_agent', 'ip_address', 'http_accept')
#         })
#     )

# admin.site.register(AccessLog, AccessLogAdmin)


# def admin_login(request, extra_context=None):
#     """Redirect to default login view which enforces auth policy."""
#     next_page = request.get_full_path()
#     next_url = next_page.split('=')[1] if '=' in next_page else next_page
#     q = REDIRECT_FIELD_NAME + '=' + next_url
#     return HttpResponseRedirect(reverse('login') + '?' + q)


# admin.site.login = admin_login


# def admin_logout(request, extra_context=None):
#     """Redirect to default login page and not /admin area."""
#     return HttpResponseRedirect(reverse('login'))


# admin.site.logout = admin_logout
