"""Command line for access borrowed from axes."""
from django.core.management.base import BaseCommand

from access.models import AccessAttempt


class Command(BaseCommand):
    """Main command class."""

    args = ''
    help = ("List login attempts")

    def handle(self, *args, **kwargs):
        """Main entry."""
        for at in AccessAttempt.objects.all():
            print ("%s %s %s" % (at.ip_address, at.username, at.failures))
