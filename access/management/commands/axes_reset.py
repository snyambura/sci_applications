"""Command line for access borrowed from axes."""
from django.core.management.base import BaseCommand

from access.utils import reset


class Command(BaseCommand):
    """This is the command."""

    args = ''
    help = ("resets any lockouts or failed login records. If called with an "
            "IP, resets only for that IP")

    def handle(self, *args, **kwargs):
        """Main handler."""
        count = 0
        if args:
            for ip in args:
                count += reset(ip=ip)
        else:
            count = reset()

        if count:
            print('{0} attempts removed.'.format(count))
        else:
            print('No attempts found.')
