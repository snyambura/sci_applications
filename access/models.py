"""Access handler models."""
from django.db import models
from django.utils import six


class CommonAccess(models.Model):
    """Common access class."""

    user_agent = models.CharField(
        max_length=255,
    )

    ip_address = models.GenericIPAddressField(
        verbose_name='IP Address',
        null=True,
    )

    username = models.CharField(
        max_length=255,
        null=True,
    )

    # Once a user logs in from an ip, that combination is trusted and not
    # locked out in case of a distributed attack
    trusted = models.BooleanField(
        default=False,
    )

    http_accept = models.CharField(
        verbose_name='HTTP Accept',
        max_length=1025,
    )

    path_info = models.CharField(
        verbose_name='Path',
        max_length=255,
    )

    attempt_time = models.DateTimeField(
        auto_now_add=True,
    )

    class Meta:
        """Override some values."""

        abstract = True
        ordering = ['-attempt_time']


class AccessAttempt(CommonAccess):
    """Access attempt class."""

    get_data = models.TextField(
        verbose_name='GET Data',
    )

    post_data = models.TextField(
        verbose_name='POST Data',
    )

    failures_since_start = models.PositiveIntegerField(
        verbose_name='Failed Logins',
    )

    @property
    def failures(self):
        """To return failures values."""
        return self.failures_since_start

    def __unicode__(self):
        """For the admin."""
        return six.u('Attempted Access: %s') % self.attempt_time

    class Meta:
        """Override some values."""

        db_table = 'auth_login_attempt'


class AccessLog(CommonAccess):
    """Access log class."""

    logout_time = models.DateTimeField(
        null=True,
        blank=True,
    )

    def __unicode__(self):
        """For admin."""
        return six.u('Access Log for %s @ %s') % (self.username,
                                                  self.attempt_time)

    class Meta:
        """Override some values."""

        db_table = 'auth_login_accesslog'


class AccessRequest(models.Model):
    """Model for guests access request."""

    names = models.CharField(max_length=100)
    email_address = models.EmailField(max_length=100, unique=True)
    phone_number = models.CharField(max_length=20, unique=True)
    ip_address = models.GenericIPAddressField(protocol='both')
    timestamp_requested = models.DateTimeField(auto_now=True)

    class Meta:
        """Override table details."""

        db_table = 'auth_login_request'
